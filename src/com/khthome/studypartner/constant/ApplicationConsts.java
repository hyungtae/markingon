package com.khthome.studypartner.constant;

public class ApplicationConsts {
	private ApplicationConsts(){}
	
	public static String TAG = "S.P Debug";
	public static final String ERROR_FROM_SERVER = "error from server";
	public static final String ERROR_FROM_CLIENT = "error from client";
}
