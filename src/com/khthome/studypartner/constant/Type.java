package com.khthome.studypartner.constant;

public class Type {
	private Type(){}
	
	public static final int TYPE_EX_ARITHMETIC = 001;			// ex_1,2,3,4,5
	public static final int TYPE_EX_ALPHABETICAL_1 = 011;		// ex_a,b,c,d,e
	public static final int TYPE_EX_ALPHABETICAL_2 = 012;		// ex_A,B,C,D,E
	public static final int TYPE_EX_HANGEUL_1 = 021;			// ex_ㄱ,ㄴ,ㄷ,ㄹ,ㅁ
	public static final int TYPE_EX_HANGEUL_2 = 022;			// ex_가,나,다,라,마
	public static final int TYPE_EX_CUSTOM = 031;
	
	public static final int TYPE_MULTIPLE_CHOICE = 0;
	public static final int TYPE_TOTAL_ONE = 11;
	public static final int TYPE_TOTAL_TWO = 22;
	public static final int TYPE_TOTAL_THREE = 33;
	public static final int TYPE_TOTAL_FOUR = 44;
	public static final int TYPE_TOTAL_FIVE = 55;
	
	public static final int TYPE_ESSAY_FORM = 1;
	
	public static final int TYPE_KEYBOARD_HANGUEL = 66;
	public static final int TYPE_KEYBOARD_ENGLISH = 77;
	public static final int TYPE_KEYBOARD_NUMERIC = 88;
}
