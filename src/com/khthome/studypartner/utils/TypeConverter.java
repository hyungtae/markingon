package com.khthome.studypartner.utils;

import com.khthome.studypartner.exception.MarkingTypeInvalidException;
import com.khthome.studypartner.exception.StudyPartnerException;

public class TypeConverter {
	private static TypeConverter instance;
	
	private TypeConverter(){}
	
	public synchronized static TypeConverter getInstance(){
		if(instance == null)
			instance = new TypeConverter();
		
		return instance;
	}
	
	public String convertToArithmeticTypeValue(int marking_no) throws StudyPartnerException {
		return String.valueOf(marking_no);
	}
	
	public String convertToAlphabeticalTypeOne(int marking_no) throws StudyPartnerException {
		String value = "";
		switch(marking_no){
		case 1:
			value = "a";
			break;
		case 2:
			value = "b";
			break;
		case 3:
			value = "c";
			break;
		case 4:
			value = "d";
			break;
		case 5:
			value = "e";
			break;
		default:
			try {
				throw new MarkingTypeInvalidException("Invalid AlphabeticalType");
			} catch (MarkingTypeInvalidException e) {
				// TODO Auto-generated catch block
				throw new StudyPartnerException(e);
			}
		}
		return value;
	}
	
	public String convertToAlphabeticalTypeTwo(int marking_no) throws StudyPartnerException {
		String value = "";
		switch(marking_no){
		case 1:
			value = "A";
			break;
		case 2:
			value = "B";
			break;
		case 3:
			value = "C";
			break;
		case 4:
			value = "D";
			break;
		case 5:
			value = "E";
			break;
		default:
			try {
				throw new MarkingTypeInvalidException("Invalid AlphabeticalType");
			} catch (MarkingTypeInvalidException e) {
				// TODO Auto-generated catch block
				throw new StudyPartnerException(e);
			}
		}
		return value;
	}
	
	public String convertToHangeulTypeOne(int marking_no) throws StudyPartnerException {
		String value = "";
		switch(marking_no){
		case 1:
			value = "ㄱ";
			break;
		case 2:
			value = "ㄴ";
			break;
		case 3:
			value = "ㄷ";
			break;
		case 4:
			value = "ㄹ";
			break;
		case 5:
			value = "ㅁ";
			break;
		default:
			try {
				throw new MarkingTypeInvalidException("Invalid HanguelType");
			} catch (MarkingTypeInvalidException e) {
				// TODO Auto-generated catch block
				throw new StudyPartnerException(e);
			}
		}
		return value;
	}
	
	public String convertToHangeulTypeTwo(int marking_no) throws StudyPartnerException {
		String value = "";
		switch(marking_no){
		case 1:
			value = "가";
			break;
		case 2:
			value = "나";
			break;
		case 3:
			value = "다";
			break;
		case 4:
			value = "라";
			break;
		case 5:
			value = "마";
			break;
		default:
			try {
				throw new MarkingTypeInvalidException("Invalid HanguelType");
			} catch (MarkingTypeInvalidException e) {
				// TODO Auto-generated catch block
				throw new StudyPartnerException(e);
			}
		}
		return value;
	}
	
	public String convertToCustomType(int marking_no) throws StudyPartnerException {
		String value = "";
		switch(marking_no){
		case 1:
			value = "";
			break;
		case 2:
			value = "";
			break;
		case 3:
			value = "";
			break;
		case 4:
			value = "";
			break;
		case 5:
			value = "";
			break;
		default:
			try {
				throw new MarkingTypeInvalidException("Invalid CustomType");
			} catch (MarkingTypeInvalidException e) {
				// TODO Auto-generated catch block
				throw new StudyPartnerException(e);
			}
		}
		return value;
	}
}
