package com.khthome.studypartner.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import co.juliansuarez.libwizardpager.wizard.model.ModelCallbacks;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.model.ReviewItem;

import com.khthome.studypartner.client.S1DWizardFragment;
import com.khthome.studypartner.client.S1SWizardFragment;
import com.khthome.studypartner.model.parcel.S1SPageModel;

public class S1SWizardPage extends Page {

	public S1SWizardPage(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		return S1SWizardFragment.create(getKey());
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		S1SPageModel ssm = (S1SPageModel) mData.getParcelable(S1SWizardFragment.SIMPLE_M_KEY);
		
		dest.add(new ReviewItem("Label", ssm.getLabel(), getKey()));
        dest.add(new ReviewItem("문항 수", ssm.getQuestion_total() + " 문항", getKey()));
        dest.add(new ReviewItem("제한시간", ssm.getEntire_time() + " 분", getKey()));
		
		
	}

	@Override
	public boolean isCompleted() {
		String[] sdm_array_str = new String[3];
		Boolean[] sdm_array_bool = new Boolean[3];
		S1SPageModel ssm = (S1SPageModel) mData.getParcelable(S1SWizardFragment.SIMPLE_M_KEY);
		if(ssm !=null){
			sdm_array_str[0] = ssm.getLabel();
			sdm_array_str[1] = ssm.getQuestion_total();
			sdm_array_str[2] = ssm.getEntire_time();	
			
			for(int i=0; i < sdm_array_str.length; i++){
				sdm_array_bool[i] = TextUtils.isEmpty(sdm_array_str[i]);
			}
			
			for(int i=0; i < sdm_array_bool.length; i++){
				if(sdm_array_bool[i])		// 필드가 비었으면 false를 리턴
					return false;
			}
			
			return true;
		}else{
			return false;
		}
		
	}
	
}
