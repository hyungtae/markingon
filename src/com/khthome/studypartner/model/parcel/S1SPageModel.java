package com.khthome.studypartner.model.parcel;

import android.os.Parcel;
import android.os.Parcelable;

public class S1SPageModel implements Parcelable {
	private String label;
	private String question_total;
	private String entire_time;
	
	public S1SPageModel() {}
   
    public S1SPageModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	label = in.readString();
    	question_total = in.readString();
    	entire_time = in.readString();
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(label);
        dest.writeString(question_total);
        dest.writeString(entire_time);
	}
	
	public static final Parcelable.Creator<S1SPageModel> CREATOR = new Parcelable.Creator<S1SPageModel>() {
        public S1SPageModel createFromParcel(Parcel in) {
             return new S1SPageModel(in);
       }

       public S1SPageModel[] newArray(int size) {
            return new S1SPageModel[size];
       }
   };

	public void setLabel(String label) {
		this.label = label;
	}

	public void setQuestion_total(String question_total) {
		this.question_total = question_total;
	}

	public void setEntire_time(String entire_time) {
		this.entire_time = entire_time;
	}

	public String getLabel() {
		return label;
	}

	public String getQuestion_total() {
		return question_total;
	}

	public String getEntire_time() {
		return entire_time;
	}

}
