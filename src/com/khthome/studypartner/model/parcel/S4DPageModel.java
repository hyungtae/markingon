package com.khthome.studypartner.model.parcel;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

public class S4DPageModel implements Parcelable {
	public ArrayList<S2DQuestionModel> list = new ArrayList<S2DQuestionModel>();
	public ArrayList<S3DQuestionHeaderModel> headerList = new ArrayList<S3DQuestionHeaderModel>();
	public ArrayList<Integer> bindedSet_list = new ArrayList<Integer>();
	public HashMap<String, ArrayList<Integer>> groupMap = new HashMap<String, ArrayList<Integer>>();	// 그룹핑 데이타를 담고 있는 리스트(ArrayList<Integer> = bindedSet_list)
	
	public String curSection = "";
	public boolean isGrouping = false;
	
	public S4DPageModel(){}
   
    public S4DPageModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	in.readTypedList(list, S2DQuestionModel.CREATOR);
    	in.readTypedList(headerList, S3DQuestionHeaderModel.CREATOR);
    	bindedSet_list = (ArrayList<Integer>)in.readSerializable();
    	curSection = in.readString();
    	isGrouping  = (in.readInt() == 0) ? false : true;
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(list);
		dest.writeTypedList(headerList);
		dest.writeSerializable(bindedSet_list);
		dest.writeString(curSection);
        dest.writeInt(isGrouping ? 1 : 0);
	}
	
	public static final Parcelable.Creator<S4DPageModel> CREATOR = new Parcelable.Creator<S4DPageModel>() {
        public S4DPageModel createFromParcel(Parcel in) {
             return new S4DPageModel(in);
       }

       public S4DPageModel[] newArray(int size) {
            return new S4DPageModel[size];
       }
   };

}
