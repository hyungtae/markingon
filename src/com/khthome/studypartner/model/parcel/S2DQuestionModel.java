package com.khthome.studypartner.model.parcel;

import static com.khthome.studypartner.client.CircleGroupingNumberView.STATE_GROUPING_BEFORE;
import android.os.Parcel;
import android.os.Parcelable;

public class S2DQuestionModel implements Parcelable {
	public int qNo;							// 문제번호
	public boolean selected;				// 선택되었는지(문항을 2개 선택하여 섹션설정할 때 쓰임)
	public boolean enabled = true;			// 클릭할 수 있는지 (센션설정할 때 쓰임)
	public int answer_type;					// 해당 문항의 답란 타입 (ex_객관식, 주관식 타입 )
	public String answer_ex_type;			// 해당 문항의 보기 타입 (ex_12345, abcde, ㄱㄴㄷㄹㅁ, 가나다라마 등...)
	public int aet_spinner_position;		// 문항의 보기 타입 스피너의 position
	public int answer_ex_total;				// 해당 문항의 보기의 개수(객관식)
	public int answer_total;				// 해당 문항의 정답의 개수(객관식)
	public int keyboard_type = -1;				// 펼쳐지는 키보드 타입(주관식 항목)
	public String mc_question_type = "";		// 문제 유형(객관식)
	public String ef_question_type = "";		// 문제 유형(주관식)
	public boolean typeChanged = false;			// 다이얼로그화면에서 이전타입과 달라졌는지
	public int position = -1;
	public int state = STATE_GROUPING_BEFORE;
	public String sectionName = "";
	public String sectionName_pk = "";		// 섹션고유 이름(동일한 섹션 이름일 경우, pkName으로 구분)
	public String tag = "null";
	
	public S2DQuestionModel() {}
   
    public S2DQuestionModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	qNo = in.readInt();
    	selected  = (in.readInt() == 0) ? false : true;
    	enabled = (in.readInt() == 0) ? false : true;
    	answer_type = in.readInt();
    	answer_ex_type = in.readString();
    	aet_spinner_position = in.readInt();
    	answer_ex_total = in.readInt();
    	answer_total = in.readInt();
    	keyboard_type = in.readInt();
    	mc_question_type = in.readString();
    	ef_question_type = in.readString();
    	typeChanged = (in.readInt() == 0) ? false : true;
    	position = in.readInt();
    	state = in.readInt();
    	sectionName  = in.readString();
    	sectionName_pk = in.readString();
    	tag = in.readString();
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(qNo);
		dest.writeInt(selected ? 1 : 0);
		dest.writeInt(enabled ? 1 : 0);
		dest.writeInt(answer_type);
		dest.writeString(answer_ex_type);
		dest.writeInt(aet_spinner_position);
		dest.writeInt(answer_ex_total);
		dest.writeInt(answer_total);
		dest.writeInt(keyboard_type);
		dest.writeString(mc_question_type);
		dest.writeString(ef_question_type);
		dest.writeInt(typeChanged ? 1 : 0);
		dest.writeInt(position);
		dest.writeInt(state);
		dest.writeString(sectionName);
		dest.writeString(sectionName_pk);
		dest.writeString(tag);
	}
	
	public static final Parcelable.Creator<S2DQuestionModel> CREATOR = new Parcelable.Creator<S2DQuestionModel>() {
        public S2DQuestionModel createFromParcel(Parcel in) {
             return new S2DQuestionModel(in);
       }

       public S2DQuestionModel[] newArray(int size) {
            return new S2DQuestionModel[size];
       }
   };
}
