package com.khthome.studypartner.model.parcel;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
// 수정해야함(걍 S3Dpage 복사한거임)
public class S2DPageModel implements Parcelable {
	public ArrayList<S2DQuestionModel> list = new ArrayList<S2DQuestionModel>();
	
	public S2DPageModel() {
		// initialization
		list = new ArrayList<S2DQuestionModel>();
	}
   
    public S2DPageModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	in.readTypedList(list, S2DQuestionModel.CREATOR);
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(list);
	}
	
	public static final Parcelable.Creator<S2DPageModel> CREATOR = new Parcelable.Creator<S2DPageModel>() {
        public S2DPageModel createFromParcel(Parcel in) {
             return new S2DPageModel(in);
       }

       public S2DPageModel[] newArray(int size) {
            return new S2DPageModel[size];
       }
   };

}
