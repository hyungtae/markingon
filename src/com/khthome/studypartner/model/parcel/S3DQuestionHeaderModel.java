package com.khthome.studypartner.model.parcel;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class S3DQuestionHeaderModel implements Parcelable, Cloneable {
	public ArrayList<S2DQuestionModel> qmList = new ArrayList<S2DQuestionModel>();
	
	public String sectionName;
	
	public S3DQuestionHeaderModel() {
		// initialization
		qmList = new ArrayList<S2DQuestionModel>();
	}
   
    public S3DQuestionHeaderModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
        in.readTypedList(qmList, S2DQuestionModel.CREATOR);
        sectionName = in.readString();
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(qmList);
		dest.writeString(sectionName);
	}
	
	public static final Parcelable.Creator<S3DQuestionHeaderModel> CREATOR = new Parcelable.Creator<S3DQuestionHeaderModel>() {
        public S3DQuestionHeaderModel createFromParcel(Parcel in) {
             return new S3DQuestionHeaderModel(in);
       }

       public S3DQuestionHeaderModel[] newArray(int size) {
            return new S3DQuestionHeaderModel[size];
       }
    };
   
	@Override
	public Object clone() throws CloneNotSupportedException {
	   S3DQuestionHeaderModel qhm = (S3DQuestionHeaderModel)super.clone();
		qhm.qmList = (ArrayList<S2DQuestionModel>)qmList.clone();
		return qhm;
	}
   
}
