package com.khthome.studypartner.model.parcel;

import android.os.Parcel;
import android.os.Parcelable;

public class S1DPageModel implements Parcelable {
	private String label;
	private String more_info;
	private String question_total;
	private String entire_time;
	private String answerExTotal;				// 객관식 보기 수
	private String publishing_company;
	private String category;
	private String answerExType;				// 객관식 보기 유형
	private String custom_category;
	private boolean categorySelected;		// boolean
	private boolean answerTypeSelected;
	private int c_position;					// category spinner position
	private int a_position;					// answertype spinner position
	
	private boolean flagForReset1;
	private boolean flagForReset2;
	
	public S1DPageModel() {}
   
    public S1DPageModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	label = in.readString();
    	more_info = in.readString();
    	question_total = in.readString();
    	entire_time = in.readString();
    	answerExTotal = in.readString();
    	publishing_company = in.readString();
    	category = in.readString();
    	answerExType = in.readString();
    	custom_category = in.readString();
    	categorySelected  = (in.readInt() == 0) ? false : true;
    	answerTypeSelected  = (in.readInt() == 0) ? false : true;
    	c_position = in.readInt();
    	a_position = in.readInt();
    	flagForReset1  = (in.readInt() == 0) ? false : true;
    	flagForReset2  = (in.readInt() == 0) ? false : true;
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(label);
		dest.writeString(more_info);
        dest.writeString(question_total);
        dest.writeString(entire_time);
        dest.writeString(answerExTotal);
        dest.writeString(publishing_company);
        dest.writeString(category);
        dest.writeString(answerExType);
        dest.writeString(custom_category);
        dest.writeInt(categorySelected ? 1 : 0);
        dest.writeInt(answerTypeSelected ? 1 : 0);
        dest.writeInt(c_position);
        dest.writeInt(a_position);
        dest.writeInt(flagForReset1 ? 1 : 0);
        dest.writeInt(flagForReset2 ? 1 : 0);
	}
	
	public static final Parcelable.Creator<S1DPageModel> CREATOR = new Parcelable.Creator<S1DPageModel>() {
        public S1DPageModel createFromParcel(Parcel in) {
             return new S1DPageModel(in);
       }

       public S1DPageModel[] newArray(int size) {
            return new S1DPageModel[size];
       }
   };

	public void setLabel(String label) {
		this.label = label;
	}
	
	public void setMore_info(String more_info) {
		this.more_info = more_info;
	}

	public void setQuestion_total(String question_total) {
		this.question_total = question_total;
	}

	public void setEntire_time(String entire_time) {
		this.entire_time = entire_time;
	}
	
	public void setAnswerExTotal(String total){
		this.answerExTotal = total;
	}

	public void setPublishing_company(String publishing_company) {
		this.publishing_company = publishing_company;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public void setAnswerExType(String answerExType) {
		this.answerExType = answerExType;
	}

	public void setCustom_category(String custom_category) {
		this.custom_category = custom_category;
	}
	
	public void setCategorySelected(boolean selected) {
		this.categorySelected = selected;
	}
	
	public void setAnswerTypeSelected(boolean selected) {
		this.answerTypeSelected = selected;
	}

	public void setCategorySpinnerPosition(int position) {
		this.c_position = position;
	}
	
	public void setAnswerTypeSpinnerPosition(int position) {
		this.a_position = position;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getMore_info() {
		return more_info;
	}

	public String getQuestion_total() {
		return question_total;
	}

	public String getEntire_time() {
		return entire_time;
	}
	
	public String getAnswerExTotal(){
		return answerExTotal;
	}

	public String getPublishing_company() {
		return publishing_company;
	}

	public String getCategory() {
		return category;
	}

	public String getAnswerExType() {
		return answerExType;
	}
	
	public String getCustom_category() {
		return custom_category;
	}

	public boolean isCategorySelected() {
		return categorySelected;
	}
	
	public boolean isAnswerTypeSelected() {
		return answerTypeSelected;
	}
	
	public int getCategorySpinnerPosition() {
		return c_position;
	}
	
	public int getAnswerTypeSpinnerPosition() {
		return a_position;
	}

	public boolean getFlagForResetP3P4() {
		return flagForReset1;
	}

	public void setFlagForResetP3P4(boolean flagForReset) {
		this.flagForReset1 = flagForReset;
	}
	
	public boolean getFlagForResetP2P3() {
		return flagForReset2;
	}

	public void setFlagForResetP2P3(boolean flagForReset) {
		this.flagForReset2 = flagForReset;
	}

}
