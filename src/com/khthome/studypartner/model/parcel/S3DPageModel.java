package com.khthome.studypartner.model.parcel;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class S3DPageModel implements Parcelable {
	public ArrayList<S2DQuestionModel> list = new ArrayList<S2DQuestionModel>();
	public ArrayList<S3DQuestionHeaderModel> headerList = new ArrayList<S3DQuestionHeaderModel>();
	public ArrayList<Integer> bindedSet_list = new ArrayList<Integer>();
	
	public int startIdx = 0;
	public int prevHeaderIdx;
	
	public S3DPageModel() {
		// initialization
		list = new ArrayList<S2DQuestionModel>();
		headerList = new ArrayList<S3DQuestionHeaderModel>();
	}
   
    public S3DPageModel(Parcel in) {
       readFromParcel(in);
    }
    
    private void readFromParcel(Parcel in){
    	in.readTypedList(list, S2DQuestionModel.CREATOR);
    	in.readTypedList(headerList, S3DQuestionHeaderModel.CREATOR);
    	bindedSet_list = (ArrayList<Integer>)in.readSerializable();
    	startIdx = in.readInt();
    	prevHeaderIdx = in.readInt();
    }
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(list);
		dest.writeTypedList(headerList);
		dest.writeSerializable(bindedSet_list);
		dest.writeInt(startIdx);
		dest.writeInt(prevHeaderIdx);
	}
	
	public static final Parcelable.Creator<S3DPageModel> CREATOR = new Parcelable.Creator<S3DPageModel>() {
        public S3DPageModel createFromParcel(Parcel in) {
             return new S3DPageModel(in);
       }

       public S3DPageModel[] newArray(int size) {
            return new S3DPageModel[size];
       }
   };

}
