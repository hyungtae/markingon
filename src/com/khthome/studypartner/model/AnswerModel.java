package com.khthome.studypartner.model;

import java.util.ArrayList;

public class AnswerModel implements Cloneable{
	private ArrayList<Integer> marking_list = new ArrayList<Integer>();	// 마킹 리스트
	
	private int question_no;		// 해당 문항
	private int answer_type;		// 해당 문항의 답란 타입 (ex_객관식, 객관식다중선택 타입, 주관식 타입 )
	private String ef_answer;		// 해당 문항 주관식 정답
	private int total_answer_count;	// 정답의 개수(객관식)
	private int keyboard_type;		// 펼쳐지는 키보드 타입(주관식)
	
	public void setMarkingList(ArrayList<Integer> marking_list) {
		this.marking_list = marking_list;
	}
	
	public ArrayList<Integer> getMarkingList() {
		return marking_list;
	}
	
	public void setQuestionNo(int question_no) {
		this.question_no = question_no;
	}
	
	public int getQuestionNo() {
		return question_no;
	}
	
	public void setAnswerType(int answer_type) {
		this.answer_type = answer_type;
	}
	
	public int getAnswerType() {
		return answer_type;
	}

	public void setEssayFormAnswer(String ef_answer){
		this.ef_answer = ef_answer;
	}
	
	public String getEssayFormAnswer(){
		return ef_answer;
	}
	
	public void setTotalAnswerCount(int total_answer_count){
		this.total_answer_count = total_answer_count;
	}
	
	public int getTotalAnswerCount(){
		return total_answer_count;
	}
	
	public void setKeyBoardType(int keyboard_type){
		this.keyboard_type = keyboard_type;
	}
	
	public int getKeyBoardType(){
		return keyboard_type;
	}
	
}
