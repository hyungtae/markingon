package com.khthome.studypartner.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import co.juliansuarez.libwizardpager.wizard.model.ModelCallbacks;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.model.ReviewItem;

import com.khthome.studypartner.client.S2DWizardFragment;
import com.khthome.studypartner.model.parcel.S2DPageModel;

public class S2DWizardPage extends Page {

	public S2DWizardPage(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		return S2DWizardFragment.create(getKey());
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		S2DPageModel pm = (S2DPageModel) mData.getParcelable(S2DWizardFragment.DETAIL_STEP2_M_KEY);
		
		dest.add(new ReviewItem("문항 세부설정", "수정하러 가기", getKey()));
		
	}

	@Override
	public boolean isCompleted() {
		return super.isCompleted();
	}
	
}
