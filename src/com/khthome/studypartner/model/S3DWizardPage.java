package com.khthome.studypartner.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import co.juliansuarez.libwizardpager.wizard.model.ModelCallbacks;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.model.ReviewItem;

import com.khthome.studypartner.client.S3DWizardFragment;

public class S3DWizardPage extends Page {

	public S3DWizardPage(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		return S3DWizardFragment.create(getKey());
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
        
	}

	@Override
	public boolean isCompleted() {
		return super.isCompleted();
	}
	
}
