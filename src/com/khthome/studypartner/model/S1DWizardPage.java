package com.khthome.studypartner.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import co.juliansuarez.libwizardpager.wizard.model.ModelCallbacks;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.model.ReviewItem;

import com.khthome.studypartner.client.S1DWizardFragment;
import com.khthome.studypartner.client.S1SWizardFragment;
import com.khthome.studypartner.model.parcel.S1DPageModel;

public class S1DWizardPage extends Page {

	public S1DWizardPage(ModelCallbacks callbacks, String title) {
		super(callbacks, title);
	}

	@Override
	public Fragment createFragment() {
		return S1DWizardFragment.create(getKey());
	}

	@Override
	public void getReviewItems(ArrayList<ReviewItem> dest) {
		S1DPageModel ssm = (S1DPageModel) mData.getParcelable(S1DWizardFragment.DETAIL_M_KEY);
		
		dest.add(new ReviewItem("Label", ssm.getLabel(), getKey()));
		dest.add(new ReviewItem("추가정보", ssm.getMore_info(), getKey()));
        dest.add(new ReviewItem("문항 수", ssm.getQuestion_total() + " 문항", getKey()));
        dest.add(new ReviewItem("제한시간", ssm.getEntire_time() + " 분", getKey()));
        dest.add(new ReviewItem("출판사", ssm.getPublishing_company(), getKey()));
        if(!TextUtils.isEmpty(ssm.getCustom_category())){
        	dest.add(new ReviewItem("카테고리", ssm.getCustom_category(), getKey()));
        }else{
        	dest.add(new ReviewItem("카테고리", ssm.getCategory(), getKey()));
        }
        
	}

	@Override
	public boolean isCompleted() {
		String[] sdm_array_str = new String[9];
		Boolean[] sdm_array_bool = new Boolean[9];
		S1DPageModel sdm = (S1DPageModel) mData.getParcelable(S1DWizardFragment.DETAIL_M_KEY);
		if(sdm !=null){
			sdm_array_str[0] = sdm.getLabel();
			sdm_array_str[1] = sdm.getMore_info();
			sdm_array_str[2] = sdm.getQuestion_total();
			sdm_array_str[3] = sdm.getEntire_time();
			sdm_array_str[4] = sdm.getAnswerExTotal();
			sdm_array_str[5] = sdm.getAnswerExType();
			sdm_array_str[6] = sdm.getPublishing_company();
			sdm_array_str[7] = sdm.getCategory();			
			sdm_array_str[8] = sdm.getCustom_category();	
			
			for(int i=0; i < sdm_array_str.length; i++){
				sdm_array_bool[i] = TextUtils.isEmpty(sdm_array_str[i]);
			}
			
			for(int i=0; i < sdm_array_bool.length; i++){
				if(i == 8 && sdm_array_bool[i]){
					continue;
				}
				
				if(sdm_array_bool[i])		// 필드가 비었으면 false를 리턴
					return false;
			}
			
			return true;
		}else{
			return false;
		}
	}
	
}
