package com.khthome.studypartner.model;

import java.util.ArrayList;

public class AnswerSheetModel {
	private ArrayList<AnswerModel> answer_data_list;	
	
	private int question_total;			// 총 문항 수
	private int mcq_total;				// 객관식 총 문항 수
	private int efq_total;				// 주관식 총 문항 수
	private int answer_no_count;			// 각 문항 답란의 수
	private String category;				// 카테고리(ex_영어)
	private String title;				// 제목(ex_모의고사 제3회)
	private String publishing_company;	// 문제지 출판사 (ex_해커스토익)
	private String author;				// 지은이
	private int entire_time;				// 총 소요시간
	private int answerSheet_type;		// 답안지 타입
	private String created_by;			// 만든 사람
	private long created_at;				// 만든 시간
	
	public void setAnswerDataList(ArrayList<AnswerModel> answer_data_list) { this.answer_data_list = answer_data_list; }
	public ArrayList<AnswerModel> getAnswerDataList() { return answer_data_list; }
	
	public void setQuestionTotal(int question_total) { this.question_total = question_total; }
	public int getQuestionTotal() { return question_total; }
	
	public void setMultiChoiceQuestionTotal(int mcq_total) { this.mcq_total = mcq_total; }
	public int getMultiChoiceQuestionTotal() { return mcq_total; }
	
	public void setEssayFormQuestionTotal(int efq_total) { this.efq_total = efq_total; }
	public int getsetEssayFormQuestionTotal() { return efq_total; }
	
	public void setAnswerNoCount(int answer_no_count) { this.answer_no_count = answer_no_count; }
	public int getAnswerNoCount() { return answer_no_count; }
	
	public void setCategory(String category) { this.category = category; }
	public String getCategory() { return category; }
	
	public void setTitle(String title) { this.title = title; }
	public String getTitle() { return title; }
	
	public void setPublishingCompany(String publishing_company) { this.publishing_company = publishing_company; }
	public String getPublishingCompany() { return publishing_company; }
	
	public void setAuthor(String author) { this.author = author; }
	public String getAuthor() { return author; }
	
	public void setEntireTime(int entire_time) { this.entire_time = entire_time; }
	public int getEntireTime() { return entire_time; }
	
	public void setAnswerSheetType(int answerSheet_type) { this.answerSheet_type = answerSheet_type; }
	public int getAnswerSheetType() { return answerSheet_type; }
	
	public void setCreatedBy(String created_by) { this.created_by = created_by; }
	public String getCreatedBy() { return created_by; }
	
	public void setCreatedAt(long created_at) { this.created_at = created_at; }
	public long getCreatedAt() { return created_at; }
	
}
