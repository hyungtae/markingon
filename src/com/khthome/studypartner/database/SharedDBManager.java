package com.khthome.studypartner.database;

import java.util.ArrayList;

import com.khthome.studypartner.model.AnswerModel;

public class SharedDBManager {
	public static SharedDBManager instance;
	public ArrayList<AnswerModel> answer_data_list;
	
	public synchronized static SharedDBManager getInstance(){
		if(instance == null){
			instance = new SharedDBManager();
			
			instance.answer_data_list = new ArrayList<AnswerModel>();
		}
		
		return instance;
	}
	
	public void logout(){
		instance = null;
	}
}
