package com.khthome.studypartner.client;

import android.content.Context;
import co.juliansuarez.libwizardpager.wizard.model.AbstractWizardModel;
import co.juliansuarez.libwizardpager.wizard.model.BranchPage;
import co.juliansuarez.libwizardpager.wizard.model.PageList;

import com.khthome.studypartner.model.S1DWizardPage;
import com.khthome.studypartner.model.S1SWizardPage;
import com.khthome.studypartner.model.S2DWizardPage;
import com.khthome.studypartner.model.S3DWizardPage;
import com.khthome.studypartner.model.S4DWizardPage;

public class AnswerSheetSettingWizard extends AbstractWizardModel{

	public AnswerSheetSettingWizard(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected PageList onNewRootPageList() {
		return new PageList(new BranchPage(this, "Custom AnswerSheet")
				.addBranch("Simple Setting", new S1SWizardPage(this, "Step 1. info").setRequired(true))
				.addBranch("Detail Setting", new S1DWizardPage(this, "Step 1. info").setRequired(true),
						// 추가할 페이지는 이곳에
						new S2DWizardPage(this, "Step 2. Detail Setting"),
						new S3DWizardPage(this, "Step 3. Section Setting"),
						new S4DWizardPage(this, "Step 4. Grouping"))
						.setRequired(true)
						);
	}

}
