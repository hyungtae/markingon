package com.khthome.studypartner.client;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import co.juliansuarez.libwizardpager.wizard.model.AbstractWizardModel;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.ui.PageFragmentCallbacks;
import co.juliansuarez.libwizardpager.wizard.ui.ReviewFragment;

import com.khthome.studypartner.R;
import com.khthome.studypartner.model.S1DWizardPage;
import com.khthome.studypartner.model.S3DWizardPage;
import com.khthome.studypartner.model.S4DWizardPage;
import com.khthome.studypartner.model.parcel.S1DPageModel;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;
import com.khthome.studypartner.model.parcel.S3DPageModel;
import com.khthome.studypartner.model.parcel.S3DQuestionHeaderModel;
import com.khthome.studypartner.model.parcel.S4DPageModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.OnHeaderClickListener;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.OnHeaderLongClickListener;
/*
 * StepThreeDetail = S3D
 */
public class S4DWizardFragment extends Fragment implements OnItemClickListener,
OnHeaderClickListener, OnHeaderLongClickListener {
	public static final String ARG_KEY = "key";
	public static final String DETAIL_STEP4_M_KEY = "detail_step4_m_key";
	
	private static final String KEY_LIST_POSITION = "key_list_position";

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    private int mFirstVisible;
    
    private GridView mGridView;
    private View mFrameView;
    private GroupingGridAdapter mAdapter;
	private PageFragmentCallbacks mCallbacks;
	private ReviewFragment.Callbacks mCallbacks_r;
	private AbstractWizardModel mWizardModel;
	private String mKey;
	private S4DWizardPage mPage;
	private S3DPageModel preS3d_pm;
	private S4DPageModel pm;
	private int preQuesTotal = 0;
    private int mNumColumns = 5;
    private int mImageThumbSpacing = 0;
    private int mImageThumbSize = 0;
	
	public S4DWizardFragment(){}
	
	public static S4DWizardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        S4DWizardFragment fragment = new S4DWizardFragment();
        fragment.setArguments(args);
        return fragment;
    }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if(!(activity instanceof PageFragmentCallbacks)){
			throw new ClassCastException("Activity must implement PageFragmentCallbacks");
		}
		
		mCallbacks = (PageFragmentCallbacks) activity;
        
        if (!(activity instanceof ReviewFragment.Callbacks)) {
            throw new ClassCastException("Activity must implement ReviewFragment's callbacks");
        }

        mCallbacks_r = (ReviewFragment.Callbacks) activity;
        mWizardModel  = mCallbacks_r.onGetModel();

	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (S4DWizardPage) mCallbacks.onGetPage(mKey);
		setEmptyValueToData();
	}
	
	private void setEmptyValueToData(){

	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_step2_detail_wizard_page, container, false);
		
		return rootView;
	}
	
	@Override
    public void onDetach() {
        super.onDetach();
    }
	
	@Override
    public void onHeaderClick(AdapterView<?> parent, View view, long id) {
       
    }

    @Override
    public boolean onHeaderLongClick(AdapterView<?> parent, View view, long id) {
        
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> gridView, View view, int position, long id) {
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
        
        outState.putParcelable(DETAIL_STEP4_M_KEY, pm);
        outState.putInt("preQuesTotal", preQuesTotal); 
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.getParcelable(DETAIL_STEP4_M_KEY) != null) {
            // Restore last state for this view.
	    	pm = savedInstanceState.getParcelable(DETAIL_STEP4_M_KEY);
	    	preQuesTotal = savedInstanceState.getInt("preQuesTotal");
        }
        
	}
    
    private boolean isSkipped = false;
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		
		if (isVisibleToUser == true) { 
			setActualAdapter();
	    }else{		// 화면에서 안 보이면 어답터에서 복원하는데 필요한 parcel된 데이타를 가져옴
	    	if(mAdapter != null){
	    		pm = mAdapter.getParcelableData();
	    		
	            mPage.getData().putParcelable(DETAIL_STEP4_M_KEY, mAdapter.getParcelableData());	
	            mPage.getData().putInt("preQuesTotal", preQuesTotal);
	    	}
        }
	}
	
	private void setActualAdapter(){
		if(mGridView == null){		// pager strip을 건떠뛰고 클릭했을 경우
			isSkipped = true;
    		return;
    	}
		int curQuesTotal = 0;
		
		for (Page page : mWizardModel.getCurrentPageSequence()) {
			if(page instanceof S1DWizardPage){
				S1DPageModel pm_s1d = (S1DPageModel) page.getData().getParcelable(S1DWizardFragment.DETAIL_M_KEY);
				curQuesTotal = Integer.parseInt(pm_s1d.getQuestion_total());
				
				if(pm_s1d.getFlagForResetP3P4()){	// 특수한 상황하에 리셋해야되면(ex_총문제수 100, 초기화해서 다시 100으로 설정할 경우 이전 정보 초기화해야함)
            		pm = null;
            		pm_s1d.setFlagForResetP3P4(false);		// 리셋했으니 다시 리셋플레그 조정
            		page.getData().putParcelable(S1DWizardFragment.DETAIL_M_KEY, pm_s1d);	// 적용
            	}
			}
			
			if(page instanceof S3DWizardPage){
				S3DPageModel s3d_pm = (S3DPageModel) page.getData().getParcelable(S3DWizardFragment.DETAIL_STEP3_M_KEY);
				
				if(preS3d_pm != null && s3d_pm.startIdx != preS3d_pm.startIdx){
					pm = null;
				}
				
            	if(preQuesTotal != curQuesTotal){
            		pm = null;
            	}
				
				if(pm != null){					// 두 번째 실행부터 호출
            		mAdapter.setPreviousData(pm.list, pm.headerList, pm.bindedSet_list, pm.groupMap, pm.curSection, pm.isGrouping);
            		mAdapter.notifyDataSetChanged();
            	}else{							// 첫 실행
            		s3d_pm = initParcelData(s3d_pm);
	            	mAdapter = new GroupingGridAdapter(getActivity().getApplicationContext(), getActivity(), null, mPage);
	            	mAdapter.setPreviousData(s3d_pm.list, s3d_pm.headerList);
	            	mGridView.setAdapter(mAdapter);
	            	//mAdapter.notifyDataSetChanged();
            	}
				preS3d_pm = s3d_pm;
				preQuesTotal = curQuesTotal;
            }
            	
        }
	}
	
	private void setVirtualAdapter(){
		mAdapter = new GroupingGridAdapter(getActivity().getApplicationContext(), getActivity(), null, mPage);
        mGridView.setAdapter(mAdapter);
	}
	
	private S3DPageModel initParcelData(S3DPageModel pm_s3d){
		S3DPageModel pm = new S3DPageModel();
    	
    	for(int i=0; i<pm_s3d.headerList.size(); i++){
    		S3DQuestionHeaderModel qhm = new S3DQuestionHeaderModel();
    		qhm.sectionName = pm_s3d.headerList.get(i).sectionName;
    		for(int j=0; j<pm_s3d.headerList.get(i).qmList.size(); j++){
    			S2DQuestionModel qm = new S2DQuestionModel();
    			qm.qNo = pm_s3d.headerList.get(i).qmList.get(j).qNo;
    			qm.enabled = true;
    			qm.selected = false;
    			qm.sectionName = qhm.sectionName;
    			qm.sectionName_pk = qhm.sectionName+i;
    			
    			qhm.qmList.add(qm);		// 헤더모델에 추가
    			
    			pm.list.add(qm);		// 페이지모델에 추가
    		}
    		
    		pm.headerList.add(qhm);
    	}
    	
    	pm.startIdx = pm_s3d.startIdx;
    	
    	return pm;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mGridView = (GridView)view.findViewById(R.id.section_grid);
        mGridView.setOnItemClickListener(this);
              
        if(isSkipped){
        	setVirtualAdapter();
        	if(savedInstanceState != null){
	        	pm = savedInstanceState.getParcelable(DETAIL_STEP4_M_KEY);
		    	preQuesTotal = savedInstanceState.getInt("preQuesTotal");
        	}
        	
        	setActualAdapter();
        	isSkipped = false;
        }else{
        	setVirtualAdapter();
        }

        View frame = (View)view.findViewById(R.id.frame);

        mFrameView = frame;
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // When we get here, the size of the frame view is known.
                        // Use those dimensions to set the width of the columns and the image adapter size.
                    	if(mFrameView.getHeight()>0 && mFrameView.getWidth()>0){
                    		if (mAdapter.getNumColumns() == 0) {
                                View f = mFrameView;
                                int fh  = f.getHeight () 
                                                    - f.getPaddingTop () - f.getPaddingBottom ();
                                int shortestWidth = fh;
                                int fw = f.getWidth () - f.getPaddingLeft () - f.getPaddingRight ();
                                if (fw < shortestWidth) shortestWidth = fw;
                                int usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing
                                                  - f.getPaddingLeft () - f.getPaddingRight ();
                                //usableWidth = shortestWidth;
                                usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing;

                                int columnWidth = usableWidth / mNumColumns;
                                mImageThumbSize = columnWidth; // - mImageThumbSpacing;
                                int gridWidth = shortestWidth;

                                // The columnWidth used is an integer. That means that we usually end up with a
                                // little unused space. Fix that up with padding if the unused space is more than
                                // a half an image.
                                int estGridWidth = mNumColumns * columnWidth;
                                int unusedSpace = (shortestWidth - estGridWidth);
                                boolean addPadding = (unusedSpace * 2) > mImageThumbSize;
                                if (addPadding) {
                                   // This is not a precise calculation. Pad with roughly 1/3 the unused space.
                                   int pad = unusedSpace / 3;
                                   if (pad > 0) mGridView.setPadding (pad, pad, pad, pad);

                                }

                                 mGridView.setColumnWidth (columnWidth);
                                 Log.d("aaa", "columnWidth: "+columnWidth);
                                 
                                // Now that we have made all the extra adjustments, resize the grid
                                // and have it redo its view one more time.
                                LayoutParams lparams = new LinearLayout.LayoutParams (gridWidth, LayoutParams.MATCH_PARENT);
                                mGridView.setLayoutParams (lparams);
                                mAdapter.setNumColumns(mNumColumns);
                             }
                    	}
                        
                    	mGridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });
        
        if (savedInstanceState != null) {
            mFirstVisible = savedInstanceState.getInt(KEY_LIST_POSITION);
        }

        mGridView.setSelection(mFirstVisible);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        ((StickyGridHeadersGridView)mGridView).setOnHeaderClickListener(this);
        ((StickyGridHeadersGridView)mGridView).setOnHeaderLongClickListener(this);

        setHasOptionsMenu(true);
	}
	
	/**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mGridView.setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                    : ListView.CHOICE_MODE_NONE);
        }
    }

    @SuppressLint("NewApi")
    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            mGridView.setItemChecked(mActivatedPosition, false);
        } else {
            mGridView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

}
