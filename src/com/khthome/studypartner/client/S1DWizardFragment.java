package com.khthome.studypartner.client;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import co.juliansuarez.libwizardpager.wizard.ui.PageFragmentCallbacks;

import com.khthome.studypartner.R;
import com.khthome.studypartner.model.S1DWizardPage;
import com.khthome.studypartner.model.parcel.S1DPageModel;
/*
 * StepOneDetail = S1D
 */
public class S1DWizardFragment extends Fragment {
	public static final String ARG_KEY = "key";
	public static final String DETAIL_M_KEY = "detail_m_key";
	
	private String[] category_data = {"수능-언어", "수능-수리", "수능-외국어", "수능-사탐", "수능-과탐", "수능-직탐", "토익", "기타"};
	private String[] answerExType_data = {"숫자 (ex_1,2,3,4,5)", "알파벳1 (ex_a,b,c,d,e)", "알파벳2 (ex_A,B,C,D,E)",
			"한글1 (ex_ㄱ,ㄴ,ㄷ,ㄹ,ㅁ)", "한글2 (ex_가,나,다,라,마)"};
	
	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private S1DWizardPage mPage;
	private EditText mEditText_label;
	private EditText mEditText_moreInfo;
	private EditText mEditText_questionTotal;
	private EditText mEditText_entireTime;
	private EditText mEditText_publishing_company;
	private EditText mEditText_custom_category;
	private TextView mTextView_guideView;
	private RadioGroup mRadioGroup_answerExTotal;
	private Spinner mSpinner_answerExType;
	private Spinner mSpinner_category;
	private LayoutInflater mInflator;
	private boolean mCategorySelected;
	private boolean mAnswerTypeSelected;
	private S1DPageModel mPm;
	
	public S1DWizardFragment(){}
	
	public static S1DWizardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        S1DWizardFragment fragment = new S1DWizardFragment();
        fragment.setArguments(args);
        return fragment;
    }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if(!(activity instanceof PageFragmentCallbacks)){
			throw new ClassCastException("Activity must implement PageFragmentCallbacks");
		}
		
		mCallbacks = (PageFragmentCallbacks) activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (S1DWizardPage) mCallbacks.onGetPage(mKey);
		
		setEmptyValueToData();
		mInflator = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_step1_detail_wizard_page, container, false);
		((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
		
		S1DPageModel pm;
		
		if (savedInstanceState != null && savedInstanceState.getParcelable(DETAIL_M_KEY) != null)
			pm = savedInstanceState.getParcelable(DETAIL_M_KEY);
		else
			pm = (S1DPageModel) mPage.getData().getParcelable(DETAIL_M_KEY);
		

		mEditText_label = (EditText) rootView.findViewById(R.id.d_answerSheet_label);
		mEditText_moreInfo = (EditText) rootView.findViewById(R.id.d_answerSheet_more_info);
		mEditText_questionTotal = (EditText) rootView.findViewById(R.id.d_question_total);
		mEditText_entireTime = (EditText) rootView.findViewById(R.id.d_entire_time);
		mEditText_publishing_company = (EditText) rootView.findViewById(R.id.publisching_company);
		mEditText_custom_category = (EditText) rootView.findViewById(R.id.custom_category);
		
		mTextView_guideView = (TextView) rootView.findViewById(R.id.guideView);
		mRadioGroup_answerExTotal = (RadioGroup) rootView.findViewById(R.id.at_radioGroup);
		mRadioGroup_answerExTotal.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				switch (checkedId) {
					case R.id.at_rb1:
						mTextView_guideView.setText("2 지선다형");
						mPm.setAnswerExTotal("2");
						mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
						mPage.notifyDataChanged();
						break;
					case R.id.at_rb2:
						mTextView_guideView.setText("3 지선다형");
						mPm.setAnswerExTotal("3");
						mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
						mPage.notifyDataChanged();
						break;
					case R.id.at_rb3:
						mTextView_guideView.setText("4 지선다형");
						mPm.setAnswerExTotal("4");
						mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
						mPage.notifyDataChanged();
						break;
					case R.id.at_rb4:
						mTextView_guideView.setText("5 지선다형");
						mPm.setAnswerExTotal("5");
						mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
						mPage.notifyDataChanged();
						break;
					}
				}
			});
		
		mSpinner_answerExType = (Spinner) rootView.findViewById(R.id.answerExType_spinner);
		mSpinner_answerExType.setAdapter(answerExTypeSpinnerAdapter);
		mSpinner_category = (Spinner) rootView.findViewById(R.id.category_spinner);
		mSpinner_category.setAdapter(categorySpinnerAdapter);
		
		if(pm.isCategorySelected()){
			mCategorySelected = pm.isCategorySelected();
			mSpinner_category.setSelection(pm.getCategorySpinnerPosition());
		}
		
		if(pm.isAnswerTypeSelected()){
			mAnswerTypeSelected = pm.isAnswerTypeSelected();
			mSpinner_answerExType.setSelection(pm.getAnswerTypeSpinnerPosition());
		}
		
		mSpinner_answerExType.setOnTouchListener(answerExTypeSpinnerTouchListener);
		
		mSpinner_category.setOnItemSelectedListener(categorySelectedListener);
		mSpinner_category.setOnTouchListener(categorySpinnerTouchListener);
		
		mEditText_questionTotal.setNextFocusDownId(R.id.d_entire_time);			// code for compatability 2.3
		
		mEditText_label.setText(pm.getLabel());
		mEditText_moreInfo.setText(pm.getMore_info());
		mEditText_questionTotal.setText(pm.getQuestion_total());
		mEditText_entireTime.setText(pm.getEntire_time());
		mEditText_publishing_company.setText(pm.getPublishing_company());
		mEditText_custom_category.setText(pm.getCustom_category());
			
		return rootView;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		
		if (isVisibleToUser == true) { 			// 특수한 상황하에 리셋해야되면(ex_총문제수 100, 초기화해서 다시 100으로 설정할 경우 이전 정보 초기화해야함)
			if(mPm.getQuestion_total() == null || mPm.getQuestion_total().equals("")){
				mPm.setFlagForResetP3P4(true);
				mPm.setFlagForResetP2P3(true);
			}else{
				mPm.setFlagForResetP3P4(false);
				mPm.setFlagForResetP2P3(false);
			}
	    }
		
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		setInputType();
		
		mEditText_label.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				mPm.setLabel((editable != null) ? editable.toString() : null);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}
		});
		
		mEditText_moreInfo.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				mPm.setMore_info((editable != null) ? editable.toString() : null);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}
		});
		
		mEditText_questionTotal.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				String s = editable.toString();
		        if(!s.startsWith("0")){
		        	mPm.setQuestion_total((editable != null) ? editable.toString() : null);
					mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
					mPage.notifyDataChanged();
		        }else{
		        	mEditText_questionTotal.setText("");
		        }
			}
		});
		
		mEditText_entireTime.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				String s = editable.toString();
		        if(!s.startsWith("0")){
		        	mPm.setEntire_time((editable != null) ? editable.toString() : null);
					mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
					mPage.notifyDataChanged();
		        }else{
		        	mEditText_entireTime.setText("");
		        }
			}
		});
		
		mEditText_publishing_company.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				mPm.setPublishing_company((editable != null) ? editable.toString() : null);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}
		});
		
		mEditText_custom_category.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				mPm.setCustom_category((editable != null) ? editable.toString() : null);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
				
			}
		});
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(DETAIL_M_KEY, mPm);
    }
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
	    if (savedInstanceState != null && savedInstanceState.getParcelable(DETAIL_M_KEY) != null) {
            // Restore last state for this view.
	    	mPm = savedInstanceState.getParcelable(DETAIL_M_KEY);
        }
	}
	
	private void setEmptyValueToData(){
		mPm = new S1DPageModel(); 
		mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
	}
	
	protected void setInputType(){
		mEditText_label.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_label.setPrivateImeOptions("defaultInputmode=korean;");
		mEditText_moreInfo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_moreInfo.setPrivateImeOptions("defaultInputmode=korean;");
		mEditText_questionTotal.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_entireTime.setInputType(InputType.TYPE_CLASS_NUMBER);
		mEditText_publishing_company.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_publishing_company.setPrivateImeOptions("defaultInputmode=korean;");
		mEditText_custom_category.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_custom_category.setPrivateImeOptions("defaultInputmode=korean;");
	}
	
	private SpinnerAdapter categorySpinnerAdapter = new BaseAdapter() {
		private TextView text;
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				// convertView = mInflator.inflate(android.R.layout.simple_spinner_dropdown_item, null);
				convertView = mInflator.inflate(R.layout.row_spinner, null);
			}
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			if (!mCategorySelected) {
				text.setText("");
				text.setHint("선택하세요");
			} else {
				text.setText(category_data[position]);
				mPm.setCategory(category_data[position]);
				mPm.setCategorySelected(mCategorySelected);
				mPm.setCategorySpinnerPosition(position);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return category_data[position];
		}

		@Override
		public int getCount() {
			return category_data.length;
		}

		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflator.inflate(
						R.layout.row_spinner, null);
			}
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			text.setTextSize(TypedValue.COMPLEX_UNIT_SP , 15);
			text.setText(category_data[position]);
			
			return convertView;
		};
	};

	private OnItemSelectedListener categorySelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			if(position == category_data.length-1){
				mEditText_custom_category.setVisibility(View.VISIBLE);
				mEditText_custom_category.requestFocus();
				
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInputFromWindow(mEditText_custom_category.getApplicationWindowToken(),  InputMethodManager.SHOW_FORCED, 0); 
			}else{
				mEditText_custom_category.setVisibility(View.INVISIBLE);
				mEditText_custom_category.setText("");

				mPm.setCustom_category("");
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
	};

	private OnTouchListener categorySpinnerTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			mCategorySelected = true;
			((BaseAdapter) categorySpinnerAdapter).notifyDataSetChanged();
			return false;
		}
	};
	
	private SpinnerAdapter answerExTypeSpinnerAdapter = new BaseAdapter() {
		private TextView text;
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				// convertView = mInflator.inflate(android.R.layout.simple_spinner_dropdown_item, null);
				convertView = mInflator.inflate(R.layout.row_spinner, null);
			}
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			if (!mAnswerTypeSelected) {
				text.setText("");
				text.setHint("선택하세요");
			} else {
				text.setText(answerExType_data[position]);
				mPm.setAnswerExType(answerExType_data[position]);
				mPm.setAnswerTypeSelected(mAnswerTypeSelected);
				mPm.setAnswerTypeSpinnerPosition(position);
				mPage.getData().putParcelable(DETAIL_M_KEY, mPm);
				mPage.notifyDataChanged();
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return answerExType_data[position];
		}

		@Override
		public int getCount() {
			return answerExType_data.length;
		}

		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflator.inflate(
						R.layout.row_spinner, null);
			}
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			text.setTextSize(TypedValue.COMPLEX_UNIT_SP , 15);
			text.setText(answerExType_data[position]);
			
			return convertView;
		};
	};
	
	private OnTouchListener answerExTypeSpinnerTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			mAnswerTypeSelected = true;
			((BaseAdapter) answerExTypeSpinnerAdapter).notifyDataSetChanged();
			return false;
		}
	};

}
