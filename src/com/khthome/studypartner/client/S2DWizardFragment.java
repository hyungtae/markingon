package com.khthome.studypartner.client;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import co.juliansuarez.libwizardpager.wizard.model.AbstractWizardModel;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.ui.PageFragmentCallbacks;
import co.juliansuarez.libwizardpager.wizard.ui.ReviewFragment;

import com.khthome.studypartner.R;
import com.khthome.studypartner.constant.Type;
import com.khthome.studypartner.model.S1DWizardPage;
import com.khthome.studypartner.model.S2DWizardPage;
import com.khthome.studypartner.model.parcel.S1DPageModel;
import com.khthome.studypartner.model.parcel.S2DPageModel;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;
/*
 * StepTwoDetail = S2D
 */
public class S2DWizardFragment extends Fragment implements OnItemClickListener {
	private static final String ARG_KEY = "key";
	public static final String DETAIL_STEP2_M_KEY = "detail_step2_m_key";
	
	private static final String KEY_LIST_POSITION = "key_list_position";
	
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;
    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */

    private int mFirstVisible;
	
	private PageFragmentCallbacks mCallbacks;
	private ReviewFragment.Callbacks mCallbacks_r;
	private AbstractWizardModel mWizardModel;
	private String mKey;
	private S2DWizardPage mPage;
	private S2DPageModel pm;
	
	private GridView mGridView;
    private View mFrameView;
    private QuestionDetailGridAdapter mAdapter;
    private int preQuesTotal = 0;
    private int mNumColumns = 5;
    private int mImageThumbSpacing = 0;
    private int mImageThumbSize = 0;
    private boolean isSkipped = false;

	
	public S2DWizardFragment(){}
	
	public static S2DWizardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        S2DWizardFragment fragment = new S2DWizardFragment();
        fragment.setArguments(args);
        return fragment;
    }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if(!(activity instanceof PageFragmentCallbacks)){
			throw new ClassCastException("Activity must implement PageFragmentCallbacks");
		}
		
		mCallbacks = (PageFragmentCallbacks) activity;
		
		if (!(activity instanceof ReviewFragment.Callbacks)) {
            throw new ClassCastException("Activity must implement ReviewFragment's callbacks");
        }
		
		mCallbacks_r = (ReviewFragment.Callbacks) activity;
        mWizardModel  = mCallbacks_r.onGetModel();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (S2DWizardPage) mCallbacks.onGetPage(mKey);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_step2_detail_wizard_page, container, false);
		
		return rootView;
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
        
        outState.putParcelable(DETAIL_STEP2_M_KEY, pm);
        outState.putInt("preQuesTotal", preQuesTotal);  
    }
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        if (savedInstanceState != null && savedInstanceState.getParcelable(DETAIL_STEP2_M_KEY) != null) {
            // Restore last state for this view.
	    	pm = savedInstanceState.getParcelable(DETAIL_STEP2_M_KEY);
	    	preQuesTotal = savedInstanceState.getInt("preQuesTotal");
        }
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		// TODO Auto-generated method stub
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser == true) { 
			setActualAdapter();
	    }else{		// 화면에서 안 보이면 어답터에서 복원하는데 필요한 parcel된 데이타를 가져옴
	    	if(mAdapter != null){
	    		ActionMode am = mAdapter.getActionMode();
	    		if(am != null){			// make a CAB disappear  
	    			am.finish();
	    			mAdapter.deSelectAllItem();
	    		}
	    		
	    		pm = mAdapter.getParcelableData();
	    		
	            mPage.getData().putParcelable(DETAIL_STEP2_M_KEY, mAdapter.getParcelableData());	// Grouping 페이지에서 현재 정보를 재사용하기 위해 저장
	            mPage.getData().putInt("preQuesTotal", preQuesTotal);
	    	}
        }
	}
	
	private void setActualAdapter(){
    	if(mGridView == null){		// pager strip을 건떠뛰고 클릭했을 경우
			isSkipped = true;
    		return;
    	}
    	
		for (Page page : mWizardModel.getCurrentPageSequence()) {
			
            if(page instanceof S1DWizardPage){
            	S1DPageModel pm_s1d = (S1DPageModel) page.getData().getParcelable(S1DWizardFragment.DETAIL_M_KEY);
            	int curQuesTotal = Integer.parseInt(pm_s1d.getQuestion_total());
       
            	if(pm_s1d.getFlagForResetP2P3()){		// 특수한 상황하에 리셋해야되면(ex_총문제수 100, 초기화해서 다시 100으로 설정할 경우 이전 정보 초기화해야함)
            		pm = null;
            	}
            	
            	if(preQuesTotal != curQuesTotal){
            		pm = null;
            	}
            		
            	if(pm != null){					// 두 번째 실행부터 호출
            		mAdapter.setPreviousData(pm.list);
            		mAdapter.notifyDataSetChanged();
            	}else{								// 첫 실행
            		ArrayList<S2DQuestionModel> qmList = new ArrayList<S2DQuestionModel>();
            		for(int i = 1; i <= curQuesTotal; i++){
	            		S2DQuestionModel qm_s2d = new S2DQuestionModel();
	            		qm_s2d.qNo = i;
	            		qm_s2d.answer_type = Type.TYPE_MULTIPLE_CHOICE;
	            		qm_s2d.answer_ex_type = pm_s1d.getAnswerExType();
	            		qm_s2d.aet_spinner_position = pm_s1d.getAnswerTypeSpinnerPosition();
	            		qm_s2d.answer_ex_total = Integer.parseInt(pm_s1d.getAnswerExTotal());
	            		qm_s2d.answer_total = 1;
	                	qmList.add(qm_s2d);
	        		}
            		
            		mAdapter = new QuestionDetailGridAdapter(getActivity().getApplicationContext(), getActivity(), qmList, mPage);
            		mGridView.setAdapter(mAdapter);
            	}
            	
            	preQuesTotal = curQuesTotal;
            }
            	
        }
	}
	
	private void setVirtualAdapter(){
		mAdapter = new QuestionDetailGridAdapter(getActivity().getApplicationContext(), getActivity(), null, mPage);
        mGridView.setAdapter(mAdapter);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mGridView = (GridView)view.findViewById(R.id.section_grid);
        mGridView.setOnItemClickListener(this);
        
        if(isSkipped){
        	setVirtualAdapter();
        	pm = savedInstanceState.getParcelable(DETAIL_STEP2_M_KEY);
	    	preQuesTotal = savedInstanceState.getInt("preQuesTotal");
	    	
        	setActualAdapter();
        	isSkipped = false;
        }else{
        	setVirtualAdapter();
        }
        

        View frame = (View)view.findViewById(R.id.frame);

        mFrameView = frame;
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // When we get here, the size of the frame view is known.
                        // Use those dimensions to set the width of the columns and the image adapter size.
                    	if(mFrameView.getHeight()>0 && mFrameView.getWidth()>0){
                    		if (mAdapter.getNumColumns() == 0) {
                                View f = mFrameView;
                                int fh  = f.getHeight () 
                                                    - f.getPaddingTop () - f.getPaddingBottom ();
                                int shortestWidth = fh;
                                int fw = f.getWidth () - f.getPaddingLeft () - f.getPaddingRight ();
                                if (fw < shortestWidth) shortestWidth = fw;
                                int usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing
                                                  - f.getPaddingLeft () - f.getPaddingRight ();
                                //usableWidth = shortestWidth;
                                usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing;

                                int columnWidth = usableWidth / mNumColumns;
                                mImageThumbSize = columnWidth; // - mImageThumbSpacing;
                                int gridWidth = shortestWidth;

                                // The columnWidth used is an integer. That means that we usually end up with a
                                // little unused space. Fix that up with padding if the unused space is more than
                                // a half an image.
                                int estGridWidth = mNumColumns * columnWidth;
                                int unusedSpace = (shortestWidth - estGridWidth);
                                boolean addPadding = (unusedSpace * 2) > mImageThumbSize;
                                if (addPadding) {
                                   // This is not a precise calculation. Pad with roughly 1/3 the unused space.
                                   int pad = unusedSpace / 3;
                                   if (pad > 0) mGridView.setPadding (pad, pad, pad, pad);
                                }

                                 mGridView.setColumnWidth (columnWidth);

                                // Now that we have made all the extra adjustments, resize the grid
                                // and have it redo its view one more time.
                                LayoutParams lparams = new LinearLayout.LayoutParams (gridWidth, LayoutParams.MATCH_PARENT);
                                mGridView.setLayoutParams (lparams);
                                mAdapter.setNumColumns(mNumColumns);
                             }
                    	}
                        
                    	// mGridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });
        
        if (savedInstanceState != null) {
            mFirstVisible = savedInstanceState.getInt(KEY_LIST_POSITION);
        }

        mGridView.setSelection(mFirstVisible);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        setHasOptionsMenu(true);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
	
	/**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mGridView.setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                    : ListView.CHOICE_MODE_NONE);
        }
    }
	
	@SuppressLint("NewApi")
    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            mGridView.setItemChecked(mActivatedPosition, false);
        } else {
            mGridView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

}
