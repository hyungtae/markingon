package com.khthome.studypartner.client;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.khthome.studypartner.R;
import com.khthome.studypartner.constant.Type;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;

public class GroupQuestionDetailDialogFragment extends DialogFragment {
	private String[] answerType_data = {"숫자 (ex_1,2,3,4,5)", "알파벳1 (ex_a,b,c,d,e)", "알파벳2 (ex_A,B,C,D,E)",
			"한글1 (ex_ㄱ,ㄴ,ㄷ,ㄹ,ㅁ)", "한글2 (ex_가,나,다,라,마)"};
	
	private LinearLayout mc_layout;
    private LinearLayout ef_layout;
    private LinearLayout mc_tab;
    private LinearLayout ef_tab;
	private CheckedTextView mc_checkbox;
	private CheckedTextView ef_checkbox;
	private View leftDivider;
	private View rightDivider;
	private Spinner mSpinner_answerExType;
	private TextView guideTextView;
	private RadioGroup at_ex_radioGroup;
	private RadioGroup at_radioGroup;
	private RadioGroup ef_radioGroup;
	private EditText mc_questionType_editText;
	private EditText ef_questionType_editText;
	private Button yes_btn;
	private Button no_btn;
	private DialogListener mListener;
	private LayoutInflater mInflator;
	private ArrayList<S2DQuestionModel> list;
	private String temp_answer_ex_type;
	private boolean isCanceled;
	private boolean isShownSpinnerHint;			// "선택하세요"라는 스피너 힌트를 보여줬다면, 객관식 보기유형을 선택하지 않은 것
	private int temp_answer_type;				// 해당 문항의 답란 타입 (ex_객관식, 주관식 타입 )
	private int temp_answer_ex_total;		// 해당 문항의 보기의 개수(객관식)
	private int temp_answer_total;			// 해당 문항의 정답의 개수(객관식)
	private int temp_aet_spinner_position;	// 문항의 보기 타입 스피너의 position(객관식)
	private int temp_ef_keyboard_type;		// 입력답안 유형(주관식)
	private String toastMessage;
	private boolean selected;
	
	OnClickListener tabListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.tab_mc:
					temp_answer_type = Type.TYPE_MULTIPLE_CHOICE;
					showMutiChoiceTab();
					break;
	
				case R.id.tab_ef:
					temp_answer_type = Type.TYPE_ESSAY_FORM;
					showEssayFormTab();
					break;
			}
		}
	};
	
	OnClickListener btnListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			switch (v.getId()) {
			case R.id.btn_yes:
				
				if(!checkFields()){
					SuperToast superToast = new SuperToast(getActivity());
					superToast.setText(toastMessage);
					superToast.setDuration(SuperToast.Duration.SHORT);
					superToast.setBackground(SuperToast.Background.GREEN);
					superToast.setTextColor(Color.WHITE);
					superToast.show();
					break;
				}
				
				
				if(temp_answer_type == Type.TYPE_MULTIPLE_CHOICE){
					for(int i = 0; i < list.size(); i++){
						if(list.get(i).selected){
							list.get(i).answer_type = temp_answer_type;
							list.get(i).answer_ex_total = temp_answer_ex_total;
							list.get(i).answer_total = temp_answer_total;
							list.get(i).answer_ex_type = temp_answer_ex_type;
							list.get(i).aet_spinner_position = temp_aet_spinner_position;
							list.get(i).mc_question_type = mc_questionType_editText.getText().toString();
						}
					}
				}else if(temp_answer_type == Type.TYPE_ESSAY_FORM){
					for(int i = 0; i < list.size(); i++){
						if(list.get(i).selected){
							list.get(i).answer_type = temp_answer_type;
							list.get(i).ef_question_type = ef_questionType_editText.getText().toString();
							list.get(i).keyboard_type = temp_ef_keyboard_type;
						}
					}
				}
				
				
				adjustSpinnerDefaultSelect();
				
				mListener.onFinishEditDialog();
				isCanceled = false;
				dismiss();
				break;

			case R.id.btn_no:
				
				isCanceled = true;
				dismiss();
				break;
			}
		}
	};
	
	private void adjustSpinnerDefaultSelect(){
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).selected && list.get(i).answer_type == Type.TYPE_ESSAY_FORM){
				list.get(i).typeChanged = true;
			}else{
				list.get(i).typeChanged = false;
			}
		}
	}
	
	private boolean checkFields(){
		if(temp_answer_type == Type.TYPE_MULTIPLE_CHOICE){
			if(temp_answer_ex_total == -1){
				toastMessage = "객관식 보기 수를 입력하세요";
				return false;
			}else if(temp_answer_total == -1){
				toastMessage = "객관식 정답 수를 입력하세요";
				return false;
			}else if(isShownSpinnerHint){
				toastMessage = "객관식 보기 유형을 입력하세요";
				return false;
			}
		}else if(temp_answer_type == Type.TYPE_ESSAY_FORM){
			if(temp_ef_keyboard_type == -1){
				toastMessage = "주관식 입력답안 유형을 입력하세요";
				return false;
			}
		}
		
		return true;
	}
	
	public static GroupQuestionDetailDialogFragment newInstance() {
		GroupQuestionDetailDialogFragment fragment = new GroupQuestionDetailDialogFragment();
        return fragment;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		init();
		mInflator = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	private void init(){
		temp_answer_type = Type.TYPE_MULTIPLE_CHOICE;
		temp_answer_ex_total = -1;
		temp_answer_total = -1;
		temp_aet_spinner_position = -1;
		temp_ef_keyboard_type = -1;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
        Dialog dialog = getDialog();
        
        View view = inflater.inflate(R.layout.dialog_questiondetail_setting, container);
        mc_layout = (LinearLayout) view.findViewById(R.id.mc_layout);
        ef_layout = (LinearLayout) view.findViewById(R.id.ef_layout);
        mc_tab = (LinearLayout) view.findViewById(R.id.tab_mc);
        ef_tab = (LinearLayout) view.findViewById(R.id.tab_ef);
        mc_checkbox = (CheckedTextView) view.findViewById(R.id.mc_checkbox);
        ef_checkbox = (CheckedTextView) view.findViewById(R.id.ef_checkbox);
        leftDivider = (View) view.findViewById(R.id.leftDivider);
        rightDivider = (View) view.findViewById(R.id.rightDivider);
        guideTextView = (TextView) view.findViewById(R.id.guideView);
        at_ex_radioGroup = (RadioGroup) view.findViewById(R.id.at_ex_radioGroup);
        at_radioGroup = (RadioGroup) view.findViewById(R.id.at_radioGroup);
        ef_radioGroup = (RadioGroup) view.findViewById(R.id.ef_radioGroup);
        mSpinner_answerExType = (Spinner) view.findViewById(R.id.answerExType_spinner);
        mc_questionType_editText = (EditText) view.findViewById(R.id.mc_question_type);
        ef_questionType_editText = (EditText) view.findViewById(R.id.ef_question_type);
        yes_btn = (Button) view.findViewById(R.id.btn_yes);
        no_btn = (Button) view.findViewById(R.id.btn_no);

        mSpinner_answerExType.setAdapter(answerExTypeSpinnerAdapter);
        mSpinner_answerExType.setOnTouchListener(answerExTypeSpinnerTouchListener);
        mSpinner_answerExType.setOnItemSelectedListener(answerExTypeSelectedListener);
        
        mc_tab.setOnClickListener(tabListener);
        ef_tab.setOnClickListener(tabListener);
        yes_btn.setOnClickListener(btnListener);
        no_btn.setOnClickListener(btnListener);
        at_ex_radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.at_ex_rb1:
					guideTextView.setText("2 지선다형");
					temp_answer_ex_total = 2;
					break;
				case R.id.at_ex_rb2:
					guideTextView.setText("3 지선다형");
					temp_answer_ex_total = 3;
					break;
				case R.id.at_ex_rb3:
					guideTextView.setText("4 지선다형");
					temp_answer_ex_total = 4;
					break;
				case R.id.at_ex_rb4:
					guideTextView.setText("5 지선다형");
					temp_answer_ex_total = 5;
					break;
				}
			}
        });
        at_radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.at_rb1:
					temp_answer_total = 1;
					break;
				case R.id.at_rb2:
					temp_answer_total = 2;
					break;
				case R.id.at_rb3:
					temp_answer_total = 3;
					break;
				case R.id.at_rb4:
					temp_answer_total = 4;
					break;
				case R.id.at_rb5:
					temp_answer_total = 5;
					break;
				}
			}
        });
        
        ef_radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.ef_rb1:
					temp_ef_keyboard_type = Type.TYPE_KEYBOARD_HANGUEL;
					break;
				case R.id.ef_rb2:
					temp_ef_keyboard_type = Type.TYPE_KEYBOARD_NUMERIC;
					break;
				case R.id.ef_rb3:
					temp_ef_keyboard_type = Type.TYPE_KEYBOARD_ENGLISH;
					break;
				}
			}
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                    KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK) {
                    isCanceled = true;
                }
                
                return false;
            }
        });
        
    	dialog.setTitle("문항 세부설정 (그룹)");
        dialog.setCanceledOnTouchOutside(false);

        return view;
    }
	
	public void setListener(DialogListener listener){
		this.mListener = listener;
	}
	
	public GroupQuestionDetailDialogFragment setQuestionModelList(ArrayList<S2DQuestionModel> list){
		this.list = list;
		
		return this;
	}
	
	private void showMutiChoiceTab(){
		mc_checkbox.setChecked(true);
		ef_checkbox.setChecked(false);
		leftDivider.setVisibility(View.INVISIBLE);
		rightDivider.setVisibility(View.VISIBLE);
		mc_layout.setVisibility(View.VISIBLE);
		ef_layout.setVisibility(View.INVISIBLE);
	}
	
	private void showEssayFormTab(){
		mc_checkbox.setChecked(false);
		ef_checkbox.setChecked(true);
		leftDivider.setVisibility(View.VISIBLE);
		rightDivider.setVisibility(View.INVISIBLE);
		mc_layout.setVisibility(View.INVISIBLE);
		ef_layout.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		
		if(isCanceled)
			mListener.onCancelDialog();
	}

	private SpinnerAdapter answerExTypeSpinnerAdapter = new BaseAdapter() {
		private TextView text;
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				// convertView = mInflator.inflate(android.R.layout.simple_spinner_dropdown_item, null);
				convertView = mInflator.inflate(R.layout.row_spinner, null);
			}
			
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			
			if(!selected){
				text.setText("");
				text.setHint("선택하세요");
				isShownSpinnerHint = true;
			}else{
				text.setText(answerType_data[position]);
			}
			
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return answerType_data[position];
		}

		@Override
		public int getCount() {
			return answerType_data.length;
		}

		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflator.inflate(
						R.layout.row_spinner, null);
			}
			text = (TextView) convertView.findViewById(R.id.spinnerTarget);
			text.setTextSize(TypedValue.COMPLEX_UNIT_SP , 15);
			text.setText(answerType_data[position]);
			
			return convertView;
		};
	};
	
	private OnItemSelectedListener answerExTypeSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			
			temp_answer_ex_type = answerType_data[position];
			temp_aet_spinner_position = position;
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
	};
	
	private OnTouchListener answerExTypeSpinnerTouchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			selected = true;
			((BaseAdapter) answerExTypeSpinnerAdapter).notifyDataSetChanged();
			isShownSpinnerHint = false;
			return false;
		}
	};

	public interface DialogListener {
        void onFinishEditDialog();
        void onCancelDialog();
    }
	
}
