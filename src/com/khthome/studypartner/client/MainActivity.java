package com.khthome.studypartner.client;

import static com.khthome.studypartner.constant.ApplicationConsts.TAG;
import static com.khthome.studypartner.constant.Type.TYPE_EX_HANGEUL_1;
import net.londatiga.android.QuickAction;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khthome.studypartner.R;
import com.khthome.studypartner.database.SharedDBManager;
import com.khthome.studypartner.model.AnswerSheetModel;
import com.khthome.studypartner.utils.Rotate3dAnimation;

public class MainActivity extends BaseActivity {
	public static final int FLIP_CARD_FRONT = 1;
	public static final int FLIP_CARD_BACK = -1;
	AnswerSheetListView answerSheetlist_View;
	LinearLayout topOfList_Layout;
	LinearLayout answerType_layout;
	ViewGroup mContainer;
	AnswerSheetListAdapter adapter;
	QuickAction quickHorizontalAction;
	TextView qNo_textView = null;
	QuickActionDialog qad;
	View cardFace;
	View cardBack;
	int scroll_updown_state = 0;
	int firstIdx_onSheet;
	int lastIdx_onSheet;
	int midIdx_onSheet = 3;
	boolean is_setMaxOffset_called = false;
	boolean is_onWindowFocusChanged_called = false;
	boolean isFlipped = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD) { 
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		} else {
			ActionBar actionBar = getSupportActionBar();
			actionBar.hide(); 
		}

		setContentView(R.layout.activity_main);
		// arraylist가 static이어서 프로그램이 종료(안드로이드는 왠만해선 프로그램kill 안함)되기 이전에는 사라지지않음. 그래서 이전 list를 clear함
		SharedDBManager.getInstance().answer_data_list.clear();		
		
		mContainer = (ViewGroup) findViewById(R.id.main_activity_root);
	    cardFace = (View) findViewById(R.id.cardFront);
	    cardBack = (View) findViewById(R.id.cardBack);
		mContainer.setPersistentDrawingCache(ViewGroup.PERSISTENT_ANIMATION_CACHE);
		
		topOfList_Layout = (LinearLayout) findViewById(R.id.topOfList_Layout);
		answerType_layout = (LinearLayout)findViewById(R.id.answerType_layout);
		answerSheetlist_View = (AnswerSheetListView) findViewById(R.id.listView);
		
		AnswerSheetModel asm = new AnswerSheetModel();
		asm.setQuestionTotal(50);
		asm.setMultiChoiceQuestionTotal(50);
		asm.setEssayFormQuestionTotal(0);
		asm.setAnswerNoCount(5);
		
		addAnswerTypeViewToLayout(asm.getAnswerNoCount(), TYPE_EX_HANGEUL_1);
		
		//2번째 파라미터 - 답안지 정보, 3번째 파라미터 - 각 문항의 답안정보를 담을 리스트
		adapter = new AnswerSheetListAdapter(this, asm, SharedDBManager.getInstance().answer_data_list);
		//adapter.setAnswerSheetBackground(Color.parseColor("#7299cc00"));
		adapter.setMidIdx(midIdx_onSheet);
		answerSheetlist_View.setAdapter(adapter);
		
		answerSheetlist_View.setOnDetectScrollListener(new AnswerSheetDetectScrollListener());
		answerSheetlist_View.setOnScrollListener(new AnswerSheetScrollListener());
		answerSheetlist_View.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				parent.performHapticFeedback( HapticFeedbackConstants.LONG_PRESS );
				
				if(qad ==null)
					qad = new QuickActionDialog(getApplicationContext());
				
				QuickAction mcef_quickAction = qad.enableQuickAction(view, adapter, position);
				
				if(view instanceof LinearLayout)
					qNo_textView = (TextView)view.findViewById(R.id.qNo);
				else if(view instanceof EditText)
					qNo_textView = (TextView)((LinearLayout) view.getParent().getParent()).findViewById(R.id.qNo);
				
				qad.setQuestionNoTextView(qNo_textView);
				qNo_textView.setTextColor(Color.RED);
				mcef_quickAction.show(view, null, null, "문항을 선택하시오");
				
				return false;
			}
		});
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(!is_onWindowFocusChanged_called)
			setListItemViewHeight();
		
		is_onWindowFocusChanged_called = true;
		
		
	}
	
	private void addAnswerTypeViewToLayout(int answerNo_count, int type){
		AnswerTypeView atv = null;
		for(int i=1; i<=5; i++){
			atv = new AnswerTypeView(this,i, type);
			answerType_layout.addView(atv, new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1f));
		}
		
		for(int i=answerNo_count+1; i<=5; i++){						// 답란의 수에 따라 AnswerTypeView를 Invisible 시킴
			answerType_layout.getChildAt(i-1).setVisibility(View.INVISIBLE);
		}
	}
	
	private void setListItemViewHeight(){
		int height = topOfList_Layout.getHeight();
		adapter.setItemHeight(height);
		answerSheetlist_View.setAdapter(adapter);
	}
	
	public void onFlipBtnClick(View view){
		flipCard();
	}
	
	private void flipCard(){
		if(isFlipped){
			applyRotation(FLIP_CARD_FRONT, 0, 90);
			isFlipped = false;
		}else{
			applyRotation(FLIP_CARD_BACK, 0, -90);
			isFlipped = true;
		}
	}
	
	/**
     * Setup a new 3D rotation on the container view.
     *
     * @param position the item that was clicked to show a picture, or -1 to show the list
     * @param start the start angle at which the rotation must begin
     * @param end the end angle of the rotation
     */
    private void applyRotation(int position, float start, float end) {
        // Find the center of the container
        final float centerX = mContainer.getWidth() / 2.0f;
        final float centerY = mContainer.getHeight() / 2.0f;

        // Create a new 3D rotation with the supplied parameter
        // The animation listener is used to trigger the next animation
        final Rotate3dAnimation rotation =
                new Rotate3dAnimation(start, end, centerX, centerY, 310.0f, true);
        rotation.setDuration(250);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(position));

        mContainer.startAnimation(rotation);
    }

    /**
     * This class listens for the end of the first half of the animation.
     * It then posts a new action that effectively swaps the views when the container
     * is rotated 90 degrees and thus invisible.
     */
    private final class DisplayNextView implements Animation.AnimationListener {
        private final int mPosition;

        private DisplayNextView(int position) {
            mPosition = position;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            mContainer.post(new SwapViews(mPosition));
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    /**
     * This class is responsible for swapping the views and start the second
     * half of the animation.
     */
    private final class SwapViews implements Runnable {
        private final int mPosition;

        public SwapViews(int position) {
            mPosition = position;
        }

        public void run() {
            final float centerX = mContainer.getWidth() / 2.0f;
            final float centerY = mContainer.getHeight() / 2.0f;
            Rotate3dAnimation rotation;
            
            if (mPosition > -1) {
            	cardFace.setVisibility(View.GONE);
                cardBack.setVisibility(View.VISIBLE);
                cardBack.requestFocus();

                rotation = new Rotate3dAnimation(-90, 0, centerX, centerY, 310.0f, false);
            } else {
            	cardBack.setVisibility(View.GONE);
            	cardFace.setVisibility(View.VISIBLE);
            	cardFace.requestFocus();

                rotation = new Rotate3dAnimation(90, 0, centerX, centerY, 310.0f, false);
            }

            rotation.setDuration(250);
            rotation.setFillAfter(true);
            rotation.setInterpolator(new DecelerateInterpolator());

            mContainer.startAnimation(rotation);
        }
    }
	
	class AnswerSheetDetectScrollListener implements OnDetectScrollListener {
	
		@Override
		public void onUpScrolling() {
			// TODO Auto-generated method stub
			scroll_updown_state = AnswerSheetListView.LIST_SCROLL_UP;
		}
		
		@Override
		public void onDownScrolling() {
			// TODO Auto-generated method stub
			scroll_updown_state = AnswerSheetListView.LIST_SCROLL_DOWN;
		}
	}
	
	class AnswerSheetScrollListener implements OnScrollListener {
		View child;
		
		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			switch (scrollState) {
				case OnScrollListener.SCROLL_STATE_IDLE : 
					if(is_onWindowFocusChanged_called){
						if (answerSheetlist_View != null) {
					        int firstVisible = answerSheetlist_View.getFirstVisiblePosition()
					                - answerSheetlist_View.getHeaderViewsCount();
					        int lastVisible = answerSheetlist_View.getLastVisiblePosition()
					                - answerSheetlist_View.getHeaderViewsCount();

					        child = answerSheetlist_View.getChildAt(lastVisible - firstVisible);
					        int offset = child.getTop() + child.getMeasuredHeight() - answerSheetlist_View.getMeasuredHeight();
					        
					        smoothScroll(offset);
					        
					        
					        if(offset == 0){
					        	firstIdx_onSheet = firstVisible + 2;
					        	lastIdx_onSheet = lastVisible + 1;
					        }else if(offset == child.getMeasuredHeight()){
					        	firstIdx_onSheet = firstVisible + 2;
					        	lastIdx_onSheet = lastVisible;
					        }else{
					        	firstIdx_onSheet = firstVisible + 1;
					        	lastIdx_onSheet = lastVisible;
					        }
					        
					        midIdx_onSheet = (firstIdx_onSheet + lastIdx_onSheet) / 2; 
					        
					        Log.d(TAG, "child.getMeasuredHeight(): "+child.getMeasuredHeight());
					        Log.d(TAG, "offset: "+offset);
					        Log.d(TAG, "firstVisible, lastVisible: "+firstVisible+", "+lastVisible);
					        Log.d(TAG, "firstIdx, lastIdx: "+firstIdx_onSheet+", "+lastIdx_onSheet);
					        Log.d(TAG, "midIdx: "+midIdx_onSheet);
					        
					        adapter.setMidIdx(midIdx_onSheet);
					        
					        Log.d("DBG", "firstVisible: " +firstVisible + ", lastVisible: "+lastVisible+", offset: "+offset);
					        Log.d("DBG", "midIdx_onSheet: " +midIdx_onSheet);
					    }
					}
				
				break;
				
				case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL :
					
				break;
				
				case OnScrollListener.SCROLL_STATE_FLING : 
					
				break;
			}
		}
		
		private int previousFirstVisibleItem = 0;
	    private long previousEventTime = 0;
	    private double speed = 0;
		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub
			if (previousFirstVisibleItem != firstVisibleItem){
	            long currTime = System.currentTimeMillis();
	            long timeToScrollOneElement = currTime - previousEventTime;
	            speed = ((double)1/timeToScrollOneElement)*1000;

	            previousFirstVisibleItem = firstVisibleItem;
	            previousEventTime = currTime;

	            // Log.d("DBG", "Speed: " +speed + " elements/second, Distance: "+answerSheetlist_View.getScrollDistance());
	            
	        }
			// Log.d("DBG", "firstVisibleItem: " +firstVisibleItem + ", visibleItemCount: "+visibleItemCount);
		}
		
		// smoothScroll logic살펴봐야함 speed&distance
		public void smoothScroll(int offset){
			int listItemHeight = child.getMeasuredHeight();
			if(scroll_updown_state == AnswerSheetListView.LIST_SCROLL_DOWN){
				if(speed < 2 || answerSheetlist_View.getScrollDistance() < 30){
					if (offset < listItemHeight-listItemHeight/3) {
			        	scrollDown(offset);
			        }else{
			        	scrollUp(offset);
			        }
				}else{
					scrollDown(offset);
				}
	        	
	        }else if(scroll_updown_state == AnswerSheetListView.LIST_SCROLL_UP){
	        	offset = child.getMeasuredHeight()-offset;
	        	if (offset > 0) {
	        		scrollUp(offset);
		        }
	        }
		}
		
		public void scrollDown(int offset){
			answerSheetlist_View.smoothScrollBy(offset, 200);
		}
		
		public void scrollUp(int offset){
			answerSheetlist_View.smoothScrollBy(-offset, 200);
		}
	
	}
}
