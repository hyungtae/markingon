package com.khthome.studypartner.client;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.khthome.studypartner.R;
import com.nineoldandroids.animation.ObjectAnimator;

public class CircleGroupingNumberView extends ImageView {
	private OnClickListener dummyCallbacks = new OnClickListener() {		
		@Override
		public void onClick(View v) {}
	};
	
	private static final int PRESSED_COLOR_LIGHTUP = 255 / 25;
	private static final int PRESSED_RING_ALPHA = 75;
	private static final int DEFAULT_PRESSED_RING_WIDTH_DIP = 4;
	private static final int ANIMATION_TIME_ID = android.R.integer.config_shortAnimTime;
	
	public static final int POSITIONED_START = 0;
	public static final int POSITIONED_CENTER = 1;
	public static final int POSITIONED_END = 2;
	
	public static final int STATE_GROUPING_BEFORE = 3;
	public static final int STATE_IS_GROUPING = 4;
	public static final int STATE_GROUPING_FINISHED = 5;
	
	private int centerY;
	private int centerX;
	private int outerRadius;
	private int pressedRingRadius;

	private Path path;
	private Paint circlePaint;
	private Paint focusPaint;
	private Paint outerPaint;
	private Paint linePaint;

	private float animationProgress;

	private int pressedRingWidth;
	private int outerRingWidth;
	private int lineWidth;
	private int defaultColor = Color.BLACK;
	private int pressedColor;
	private ObjectAnimator pressedAnimator;

	private OnClickListener mListener = dummyCallbacks;
	private boolean mPerformedClick;
	private int position = -1;
	private int state = STATE_GROUPING_BEFORE;

	public CircleGroupingNumberView(Context context) {
		super(context);
		init(context, null);
	}

	public CircleGroupingNumberView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public CircleGroupingNumberView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	        	if(!isEnabled())
	        		return false;
	        	
	        	touchDown(event);
	        	break;
	        
	        case MotionEvent.ACTION_UP:
	            touchUp(event);
	            break;
	        
	        case MotionEvent.ACTION_MOVE:
	        	touchMove(event);
	        	break;
	        	
	        case MotionEvent.ACTION_CANCEL:
	        	if(state != STATE_IS_GROUPING && state != STATE_GROUPING_FINISHED){
	        		circlePaint.setColor(defaultColor);
		        	hidePressedRing();
	        	}
	        	
                break;
		}

    return true;
	}
	
	public void touchDown(MotionEvent event){
		if(state != STATE_IS_GROUPING && state != STATE_GROUPING_FINISHED){		// 그룹핑된 영역을 플리핑이나 스크롤시, press애니메이션 되는 것 방지
			circlePaint.setColor(pressedColor);
			showPressedRing();
		}
		
	}

	public void touchMove(MotionEvent event){
		// do nothing
	}
	
	public void touchUp(MotionEvent event){
		playSoundEffect(android.view.SoundEffectConstants.CLICK);
		
		if(state != STATE_IS_GROUPING){
			circlePaint.setColor(defaultColor);
			hidePressedRing();
		}
		
		mPerformedClick = true;
		mListener.onClick(this);
	}
	
	public void setOnClickListener(OnClickListener listener){
		this.mListener = listener;
	}
	
	public CircleGroupingNumberView setPosition(int position){
		this.position = position;
		
		return this;
	}
	
	public CircleGroupingNumberView setState(int state){
		this.state = state;
		
		return this;
	}
	
	public CircleGroupingNumberView setSelectedView(boolean selected){
		this.mPerformedClick = selected;
		
		return this;
	}
	
	public CircleGroupingNumberView adjustValue(boolean isGrouping){
		if(isGrouping){
			state = STATE_IS_GROUPING;
		}
		
		return this;
	}
	
	public void redraw(){		// now, not using 
		if(mPerformedClick){
			invalidate();
		}
	}

	public void drawLeftLine(){
		path.moveTo(0, centerY);
		path.lineTo(centerX, centerY);
	}
	
	public void drawRightLine(){
		path.moveTo(centerX, centerY);
		path.lineTo(centerX * 2, centerY);
	}
	
	public void drawBothLine(){
		path.moveTo(0, centerY);
		path.lineTo(centerX * 2, centerY);
	}
	
	private void drawGroupingLines(Canvas canvas){
		path.reset();

		switch(position){
			case POSITIONED_START:
				if(state == STATE_GROUPING_BEFORE || state == STATE_IS_GROUPING){			// 첫 번째 클릭(입력을 받으면)에서는 검은색 테두리만 표시
					canvas.drawCircle(centerX, centerY, outerRadius- pressedRingWidth, outerPaint);
				}else if(state == STATE_GROUPING_FINISHED){		// 두 번째 입력을 받으면 오른쪽 방향으로 선을 이음
					drawRightLine();
					canvas.drawPath(path, linePaint);
					canvas.drawCircle(centerX, centerY, outerRadius - pressedRingWidth, circlePaint);
					canvas.drawCircle(centerX, centerY, outerRadius- pressedRingWidth, outerPaint);
				}
				
				break;
			case POSITIONED_CENTER:
				if(state == STATE_GROUPING_FINISHED){
					drawBothLine();
					canvas.drawPath(path, linePaint);
					canvas.drawCircle(centerX, centerY, outerRadius - pressedRingWidth, circlePaint);
					canvas.drawCircle(centerX, centerY, outerRadius- pressedRingWidth, outerPaint);
				}

				break;
			case POSITIONED_END:
				if(state == STATE_GROUPING_FINISHED){
					drawLeftLine();
					canvas.drawPath(path, linePaint);
					canvas.drawCircle(centerX, centerY, outerRadius - pressedRingWidth, circlePaint);
					canvas.drawCircle(centerX, centerY, outerRadius- pressedRingWidth, outerPaint);
				}

				break;
		}
			
		// path.reset();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		String key = (String) getTag() != null ? (String) getTag() : "null";
		
		// 갤2, 갤2LTE에서 스크롤시 모든 뷰가 ondraw() 되는 문제가 발생해서 추가한 조건문
		if(key.equals("null")){	// 그룹핑 전
			canvas.drawCircle(centerX, centerY, pressedRingRadius + animationProgress, focusPaint);
			canvas.drawCircle(centerX, centerY, outerRadius - pressedRingWidth, circlePaint);

			if(mPerformedClick && animationProgress == 0){						
				drawGroupingLines(canvas);
				mPerformedClick = false;
			}

		}else if(key.equals("isgrouping")){
			canvas.drawCircle(centerX, centerY, outerRadius - pressedRingWidth, circlePaint);
			canvas.drawCircle(centerX, centerY, outerRadius- pressedRingWidth, outerPaint);
		}else{		// 그룹핑 후
			drawGroupingLines(canvas);
		}
		
		// super.onDraw(canvas);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		centerX = w / 2;
		centerY = h / 2;
		outerRadius = Math.min(w, h) / 2;
		pressedRingRadius = outerRadius - pressedRingWidth - pressedRingWidth / 2;
	}

	public float getAnimationProgress() {
		return animationProgress;
	}

	public void setAnimationProgress(float animationProgress) {
		this.animationProgress = animationProgress;
		this.invalidate();
	}

	public void setColor(int color) {
		this.defaultColor = color;
		this.pressedColor = getHighlightColor(color, PRESSED_COLOR_LIGHTUP);

		circlePaint.setColor(defaultColor);
		focusPaint.setColor(defaultColor);
		focusPaint.setAlpha(PRESSED_RING_ALPHA);
		
		outerPaint.setColor(Color.BLACK);
		linePaint.setColor(Color.LTGRAY);

		this.invalidate();
	}

	private void hidePressedRing() {
		pressedAnimator.setFloatValues(pressedRingWidth, 0f);
		pressedAnimator.start();
	}

	private void showPressedRing() {
		pressedAnimator.setFloatValues(animationProgress, pressedRingWidth);
		pressedAnimator.start();
	}

	private void init(Context context, AttributeSet attrs) {
		this.setFocusable(true);
		this.setScaleType(ScaleType.CENTER_INSIDE);
		setClickable(true);
		setSoundEffectsEnabled(true);
		
		path = new Path();

		circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		circlePaint.setStyle(Paint.Style.FILL);

		focusPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		focusPaint.setStyle(Paint.Style.STROKE);
		
		outerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		outerPaint.setStyle(Paint.Style.STROKE);
		
		linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setStyle(Paint.Style.STROKE);

		pressedRingWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_PRESSED_RING_WIDTH_DIP, getResources()
				.getDisplayMetrics());
		
		outerRingWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_PRESSED_RING_WIDTH_DIP, getResources()
				.getDisplayMetrics());
		
		lineWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_PRESSED_RING_WIDTH_DIP-1, getResources()
				.getDisplayMetrics());

		int color = Color.BLACK;
		if (attrs != null) {
			final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleButton);
			color = a.getColor(R.styleable.CircleButton_cb_color, color);
			pressedRingWidth = (int) a.getDimension(R.styleable.CircleButton_cb_pressed_ring_width, pressedRingWidth);
			a.recycle();
		}

		setColor(color);

		focusPaint.setStrokeWidth(pressedRingWidth);
		outerPaint.setStrokeWidth(outerRingWidth);
		linePaint.setStrokeWidth(lineWidth);
		
		final int pressedAnimationTime = getResources().getInteger(ANIMATION_TIME_ID);
		pressedAnimator = ObjectAnimator.ofFloat(this, "animationProgress", 0f, 0f);
		pressedAnimator.setDuration(pressedAnimationTime);
	}

	private int getHighlightColor(int color, int amount) {
		return Color.argb(Math.min(255, Color.alpha(color)), Math.min(255, Color.red(color) + amount),
				Math.min(255, Color.green(color) + amount), Math.min(255, Color.blue(color) + amount));
	}
	
	interface OnClickListener {
		public void onClick(View v);
	}
	
}
