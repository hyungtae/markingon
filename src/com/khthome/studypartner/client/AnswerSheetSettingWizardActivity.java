package com.khthome.studypartner.client;

import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import co.juliansuarez.libwizardpager.wizard.model.AbstractWizardModel;
import co.juliansuarez.libwizardpager.wizard.model.ModelCallbacks;
import co.juliansuarez.libwizardpager.wizard.model.Page;
import co.juliansuarez.libwizardpager.wizard.ui.PageFragmentCallbacks;
import co.juliansuarez.libwizardpager.wizard.ui.ReviewFragment;
import co.juliansuarez.libwizardpager.wizard.ui.StepPagerStrip;

import com.khthome.studypartner.R;

public class AnswerSheetSettingWizardActivity extends BaseActivity implements PageFragmentCallbacks, 
				ModelCallbacks, ReviewFragment.Callbacks {
	
	private AbstractWizardModel mWizardModel = new AnswerSheetSettingWizard(this);
	private WizardPagerAdapter mPagerAdapter;
	private List<Page> mCurrentPageSequence;
	private ViewPager mPager;
	private StepPagerStrip mStepPagerStrip;
	private Button mNextButton;
	private Button mPrevButton;
	private boolean mConsumePageSelectedEvent;
	private boolean mEditingAfterReview;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_answersheet_setting_wizard);
		
		if (savedInstanceState != null) {
			mWizardModel.load(savedInstanceState.getBundle("model"));
		}
		
		mWizardModel.registerListener(this);
		
		mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager());
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mPagerAdapter);
		mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
		mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
			
			@Override
			public void onPageStripSelected(int position) {
				// TODO Auto-generated method stub
				/*position = Math.min(mPagerAdapter.getCount() -1, position);
				
				if(mPager.getCurrentItem() != position){
					mPager.setCurrentItem(position);
				}*/
			}
		});
		
		mNextButton = (Button) findViewById(R.id.next_button);
		mPrevButton = (Button) findViewById(R.id.prev_button);
		
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
			@Override
			public void onPageSelected(int position) {
				
				mStepPagerStrip.setCurrentPage(position);

				if(mConsumePageSelectedEvent){
					mConsumePageSelectedEvent = false;
					return;
				}
				
				mEditingAfterReview  = false;
				updateBottomBar();
			}
		});
		
		mNextButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mPager.getCurrentItem() == mCurrentPageSequence.size()){
					DialogFragment dg = new DialogFragment(){
						@Override
						public Dialog onCreateDialog(Bundle savedInstanceState) {
							return new AlertDialog.Builder(getActivity())
									.setMessage("맞춤답안 설정을 완료하시겠습니까?")
									.setPositiveButton(
											"네",
											null)
									.setNegativeButton("아니오",
											null).create();
						}
					};
					dg.show(getSupportFragmentManager(), "setting_check_dialog");
				}else{
					if(mEditingAfterReview){
						mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
					}else{
						mPager.setCurrentItem(mPager.getCurrentItem() + 1);
					}
				}
			}
		});
		
		mPrevButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mPager.setCurrentItem(mPager.getCurrentItem() - 1);
			}
		});
		
		onPageTreeChanged();
		updateBottomBar();
	}
	
	private void updateBottomBar(){
		int position = mPager.getCurrentItem();
		
		if(position == mCurrentPageSequence.size()){
			mNextButton.setText("설정 완료");
			mNextButton.setBackgroundResource(R.drawable.finish_background);
			mNextButton.setTextAppearance(this, R.style.TextAppearance_AppCompat_Widget_ActionBar_Title);
		}else{
			mNextButton.setText(mEditingAfterReview ? "Review" : "Next");
			mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
			TypedValue v = new TypedValue();
			getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);	//이 속성을 가져옴(style)-> v.resourceId
			mNextButton.setTextAppearance(this, v.resourceId);
			mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
		}
		
		mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
	}

	@Override
	public void onPageDataChanged(Page page) {
		if(page.isRequired()){
			if(recalculateCutOffPage()){
				mPagerAdapter.notifyDataSetChanged();
				updateBottomBar();
			}
		}
	}

	@Override
	public void onPageTreeChanged() {
		mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
		recalculateCutOffPage();
		mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // +1 = review step
		
		mPagerAdapter.notifyDataSetChanged();
		updateBottomBar();
	}
	
	private boolean recalculateCutOffPage(){
		// 완료되지 않은 페이지를 CutOff 시킴
		int cutOffPage = mCurrentPageSequence.size() + 1;
		for(int i = 0; i < mCurrentPageSequence.size(); i++){
			Page page = mCurrentPageSequence.get(i);
			if(page.isRequired() && !page.isCompleted()){
				cutOffPage = i;
				break;
			}
		}
		
		if(mPagerAdapter.getCutOffPage() != cutOffPage){
			mPagerAdapter.setCutOffPage(cutOffPage);
			return true;
		}
		
		return false;
	}
	
	@Override
	public Page onGetPage(String key) {
		return mWizardModel.findByKey(key);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mWizardModel.unregisterListener(this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putBundle("model", mWizardModel.save());
	}

	@Override
	public AbstractWizardModel onGetModel() {
		return mWizardModel;
	}

	@Override
	public void onEditScreenAfterReview(String pageKey) {
		for(int i = mCurrentPageSequence.size() -1; i >= 0; i--){
			if(mCurrentPageSequence.get(i).getKey().equals(pageKey)){
				mConsumePageSelectedEvent = true;
				mEditingAfterReview = true;
				mPager.setCurrentItem(i);
				updateBottomBar();
				break;
			}
		}
	}
	
	public class WizardPagerAdapter extends FragmentStatePagerAdapter {
		private int mCutOffPage;
		private Fragment mPrimaryItem;

		public WizardPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			if (i >= mCurrentPageSequence.size()) {
				return new ReviewFragment();
			}

			return mCurrentPageSequence.get(i).createFragment();
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO: be smarter about this
			if (object == mPrimaryItem) {
				// Re-use the current fragment (its position never changes)
				return POSITION_UNCHANGED;
			}

			return POSITION_NONE;
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object) {
			super.setPrimaryItem(container, position, object);
			mPrimaryItem = (Fragment) object;
		}

		@Override
		public int getCount() {
			return Math.min(mCutOffPage + 1, mCurrentPageSequence == null ? 1
					: mCurrentPageSequence.size() + 1);
		}

		public void setCutOffPage(int cutOffPage) {
			if (cutOffPage < 0) {
				cutOffPage = Integer.MAX_VALUE;
			}
			mCutOffPage = cutOffPage;
		}

		public int getCutOffPage() {
			return mCutOffPage;
		}
	}
	
}
