package com.khthome.studypartner.client;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

public class AnswerSheetListView extends ListView {
	public static final int LIST_SCROLL_DOWN = 1;
	public static final int LIST_SCROLL_UP = 2;
	private OnScrollListener onScrollListener;
    private OnDetectScrollListener onDetectScrollListener;
    private int distance;
    
	public AnswerSheetListView(Context context) {
		super(context);
		onCreate(context, null, null);
	}
	
	public AnswerSheetListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		onCreate(context, attrs, null);
	}
	
	public AnswerSheetListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		onCreate(context, attrs, defStyle);
	}

    private void onCreate(Context context, AttributeSet attrs, Integer defStyle) {
		setListeners();
    }
    
	private void setListeners() {
        super.setOnScrollListener(new OnScrollListener() {

            private int oldTop;
            private int oldFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (onScrollListener != null) {
                    onScrollListener.onScrollStateChanged(view, scrollState);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (onScrollListener != null) {
                    onScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                }

                if (onDetectScrollListener != null) {
                    onDetectedListScroll(view, firstVisibleItem);
                }
            }

            private void onDetectedListScroll(AbsListView absListView, int firstVisibleItem) {
                View view = absListView.getChildAt(0);
                int top = (view == null) ? 0 : view.getTop();
                
                distance = Math.abs(top-oldTop);
                
                if (firstVisibleItem == oldFirstVisibleItem) {
                    if (top > oldTop) {
                        onDetectScrollListener.onUpScrolling();
                    } else if (top < oldTop) {
                        onDetectScrollListener.onDownScrolling();
                    }
                } else {
                    if (firstVisibleItem < oldFirstVisibleItem) {
                        onDetectScrollListener.onUpScrolling();
                    } else {
                        onDetectScrollListener.onDownScrolling();
                    }
                }
                
                oldTop = top;
                oldFirstVisibleItem = firstVisibleItem;
            }
        });
    }
	
	public int getScrollDistance(){
		return distance;
	}
	
    @Override
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    public void setOnDetectScrollListener(OnDetectScrollListener onDetectScrollListener) {
        this.onDetectScrollListener = onDetectScrollListener;
    }
    

}
