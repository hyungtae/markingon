package com.khthome.studypartner.client;

public interface OnDetectScrollListener {
    void onUpScrolling();
    void onDownScrolling();
}