package com.khthome.studypartner.client;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.github.johnpersano.supertoasts.SuperToast;
import com.khthome.studypartner.R;

public class SectionDialogFragment extends DialogFragment implements OnEditorActionListener {
	private EditText mEditText;
	private DialogListener mListener;
	private boolean isCanceled;
	
	public static SectionDialogFragment newInstance() {
		SectionDialogFragment fragment = new SectionDialogFragment();
        return fragment;
    }
	
	public void setListener(DialogListener listener){
		this.mListener = listener;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_section_setting, container);
        mEditText = (EditText) view.findViewById(R.id.setion_editText);
        
        Bundle data = getArguments();
        int[] bindedSet_array = data.getIntArray("bindedSet_list");
        
        int startIdx = Math.min(bindedSet_array[0], bindedSet_array[1]) + 1; 
		int endIdx = Math.max(bindedSet_array[0], bindedSet_array[1]) + 1;
        
        getDialog().setTitle("Section   "+ startIdx +" - " + endIdx);

        // Show soft keyboard automatically
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mEditText.setOnEditorActionListener(this);
        getDialog().setCanceledOnTouchOutside(false);
        
        
        this.getDialog().setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                    KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK) {
                    isCanceled = true;
                }
                
                return false;
            }
        });
        
        return view;
    }
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		
		if(isCanceled)
			mListener.onCancelDialog();
	}
	
	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if(EditorInfo.IME_ACTION_DONE == actionId){
			if(v.getText().toString().equals("")){
				v.requestFocus();
				SuperToast superToast = new SuperToast(getActivity());
				superToast.setText("1 글자 이상 입력하세요");
				superToast.setDuration(SuperToast.Duration.SHORT);
				superToast.setBackground(SuperToast.Background.GREEN);
				superToast.setTextColor(Color.WHITE);
				superToast.show();
				
				return false;
			}
			
			
			
			mListener.onFinishEditDialog(v);
			isCanceled = false;
			dismiss();
			
			return true;
		}
		
		return false;
	}

	public interface DialogListener {
        void onFinishEditDialog(TextView v);
        void onCancelDialog();
    }
	
}
