package com.khthome.studypartner.client;

import static com.khthome.studypartner.constant.ApplicationConsts.TAG;
import static com.khthome.studypartner.constant.Type.TYPE_ESSAY_FORM;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_ENGLISH;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_HANGUEL;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_NUMERIC;
import static com.khthome.studypartner.constant.Type.TYPE_MULTIPLE_CHOICE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_FIVE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_FOUR;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_ONE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_THREE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_TWO;

import java.util.ArrayList;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import net.londatiga.android.QuickAction.OnActionItemClickListener;
import net.londatiga.android.QuickAction.OnDismissListener;
import android.animation.LayoutTransition;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.khthome.studypartner.database.SharedDBManager;
import com.khthome.studypartner.model.AnswerModel;
import com.khthome.studypartner.utils.LongPressChecker;
import com.khthome.studypartner.utils.LongPressChecker.OnLongPressListener;

public class AnswerDataView extends View {
	private ArrayList<Drawset> drawsetList = new ArrayList<Drawset>(); 
	private Drawset bds;			// back side drawset (마킹 안 된 답안)
	private Drawset fds;			// front side drawset (마킹 된 답안)
	private RectF  roundRect;
	private int position;			// list에서 해당 뷰의 position(=행)
	private int marking_no;			// current marking number
	public boolean isMarking;
	private boolean isCalledFromRunnable;
	private int bgColor;
	private OnMarkingListener listener;
	private int touch_area_interpolation;
	private long elaspedTime;
	private int roundPx;
	private LongPressChecker mLongPressChecker;			
	private TextView qNo_textView;
	private QuickActionDialog qad;
	
	
	public AnswerDataView(AnswerSheetListAdapter adapter, Context context, int position, int marking_no, View convertView) {
		super(context);
		init(adapter, context, position, marking_no, convertView);
	}
	
	private void init(final AnswerSheetListAdapter adapter, final Context context, int position, int marking_no, final View convertView){
		this.position = position;
		this.marking_no = marking_no;
		
		if(qad == null)
			qad = new QuickActionDialog(context);
		
		mLongPressChecker = new LongPressChecker(context);
		mLongPressChecker.setOnLongPressListener(new OnLongPressListener() {
			@Override
			public void onLongPressed(int position) {

				final QuickAction mcef_quickAction = qad.enableQuickAction(convertView, adapter, position);
				
				if(qNo_textView != null)
					qNo_textView.setTextColor(Color.RED);
				mcef_quickAction.show(convertView, null, null, "문항을 선택하시오");
			}
		});

		bds = new Drawset();

		Paint bpaint = new Paint();		// back side paint
		bpaint.setAntiAlias(true);
		bpaint.setColor(Color.parseColor("#ffffff"));
		bpaint.setStyle(Paint.Style.FILL);
		bds.paint = bpaint;
		
		 roundPx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
	}

	public void setQuestionNoTextView(TextView qNo_textView){
		this.qNo_textView = qNo_textView;
		qad.setQuestionNoTextView(qNo_textView);
	}
	
	public void setOnMarkingListener(OnMarkingListener listener){
		this.listener = listener;
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		touch_area_interpolation  = (int) (getMeasuredWidth() * 0.2+ getMeasuredHeight() * 0.2) / 2;
		Log.d(TAG, "touch_area_interpolation: "+touch_area_interpolation);
		float left = (float)getMeasuredWidth()/4+getMeasuredWidth()/11;
		float top = (float)getMeasuredHeight()/4;
		float right = (float)getMeasuredWidth()/2+getMeasuredWidth()/4-getMeasuredWidth()/11;
		float bottom = (float)getMeasuredHeight()/2+getMeasuredHeight()/4;
		Log.d(TAG, "AnswerDataView's (left, top, right, bottom) : ("+ left+", "+top+", "+right+", "+bottom+")");
		RectF bRoundRect = new RectF(left, top, right, bottom);
		roundRect = bRoundRect;		
				
		bds.roundRect = bRoundRect;
		drawsetList.add(bds);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "onLayout("+changed+","+left+","+top+","+right+","+bottom+")");
		super.onLayout(changed, left, top, right, bottom);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(bgColor);	// view's background
		for(Drawset ds : drawsetList){
			if(ds.roundRect != null)
				canvas.drawRoundRect(ds.roundRect, roundPx, roundPx, ds.paint);
		}
	}
	
	public void setAnswerViewBackground(int bgColor){
		this.bgColor = bgColor;
	}
	
	private float mDownX;
	private float mDownY;
	private final float SCROLL_THRESHOLD = 10;
	private boolean isOnClick;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		mLongPressChecker.deliverMotionEvent( this, event , position);
		
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
	        case MotionEvent.ACTION_DOWN:
	        	touchDown(event);
	        	break;
	        
	        case MotionEvent.ACTION_UP:
	            touchUp(event);
	            break;
	        
	        case MotionEvent.ACTION_MOVE:
	        	touchMove(event);
	        	break;
		}

	    return true;
	}
	
	public void touchDown(MotionEvent event){
		mDownX = event.getX();
        mDownY = event.getY();
        isOnClick = true;
	}

	public void touchMove(MotionEvent event){
		// do nothing
        if (isOnClick && (Math.abs(mDownX - event.getX()) > SCROLL_THRESHOLD || Math.abs(mDownY - event.getY()) > SCROLL_THRESHOLD)) {
            Log.d(TAG, "movement detected");
            isOnClick = false;
        }
	}
	
	public void touchUp(MotionEvent event){
		if (isOnClick) {
            //TODO onClick code
        	if(mDownX > roundRect.left - touch_area_interpolation && mDownX < roundRect.left + roundRect.width() + touch_area_interpolation 
        			&& mDownY > roundRect.top - touch_area_interpolation && mDownY < roundRect.top + roundRect.height() + touch_area_interpolation){

        		if(!mLongPressChecker.isLongPress()){
        			Log.d(TAG, "click detected");
        			if(isMarking){
            			drawNothing();
            			unregistAnswer(marking_no);
            		}else{
            			drawMarkOnSheetWithAnim();
            		}
        		}
        		
        		        		
        	}
        }
	}
	
	public void drawNothing(){
		if(drawsetList.contains(fds)){
			drawsetList.remove(fds);
			isMarking = false;
			invalidate();
		}
	}
	
	public void drawMarkOnSheetWithNoAnim(){
		isCalledFromRunnable = false;
		isMarking = true;
		addMarkingDrawSetToList();
		invalidate();
	}
	
	private void drawMarkOnSheetWithAnim(){
		listener.onMarking();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				long startTime = System.currentTimeMillis();
				isCalledFromRunnable = true;
				isMarking = true;
				addMarkingDrawSetToList();
				registAnswer(marking_no);
				
				float height = roundRect.height();
        		while(true){
        			if(Math.abs(fds.roundRect.bottom - fds.roundRect.top) >= height){
        				elaspedTime = System.currentTimeMillis()-startTime;
        				break;
        			}
        			
        			fds.roundRect.bottom += 2;
    				postInvalidate();
    				try {
						Thread.sleep(7);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        			
        		}
			}
		}).start();
	}
	
	private void addMarkingDrawSetToList(){
		fds = new Drawset();
		Paint fPaint = new Paint();
		fPaint.setAntiAlias(true);
		fPaint.setColor(Color.parseColor("#ff000000"));
		fPaint.setStyle(Paint.Style.FILL);
		fds.paint = fPaint;
		if(roundRect != null)
			fds.roundRect = new RectF(roundRect);
		
		if(isCalledFromRunnable)
			fds.roundRect.bottom = roundRect.top;
		
		drawsetList.add(fds);
	}
	
	public void registAnswer(int marking_no){
		addCheckedMarkingNumberToList(marking_no);
	}
	
	public void unregistAnswer(int marking_no){
		removeCheckedMarkingNumberList(marking_no);
	}
	
	public boolean addCheckedMarkingNumberToList(Integer checked_marking_number){
		AnswerModel asm = SharedDBManager.getInstance().answer_data_list.get(position);
		
		return asm.getMarkingList().add(checked_marking_number);
	}
	
	public boolean removeCheckedMarkingNumberList(Integer checked_marking_number){
		AnswerModel asm = SharedDBManager.getInstance().answer_data_list.get(position);
		
		return asm.getMarkingList().remove(checked_marking_number);
	}
	
	public void setPosition(int position){
		this.position = position;
	}
	
	public long getMarkingAnimElapsedTime(){
		return this.elaspedTime;
	}
}
