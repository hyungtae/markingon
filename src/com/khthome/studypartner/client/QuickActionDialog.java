package com.khthome.studypartner.client;

import static com.khthome.studypartner.constant.Type.TYPE_ESSAY_FORM;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_ENGLISH;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_HANGUEL;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_NUMERIC;
import static com.khthome.studypartner.constant.Type.TYPE_MULTIPLE_CHOICE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_FIVE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_FOUR;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_ONE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_THREE;
import static com.khthome.studypartner.constant.Type.TYPE_TOTAL_TWO;
import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import net.londatiga.android.QuickAction.OnActionItemClickListener;
import net.londatiga.android.QuickAction.OnDismissListener;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.khthome.studypartner.database.SharedDBManager;
import com.khthome.studypartner.model.AnswerModel;

public class QuickActionDialog {
	private Context context;
	private QuickAction mcef_quickAction;
	private QuickAction mc_c_quickAction;
	private QuickAction ef_kt_quickAction;
	private TextView qNo_textView;
	private AnswerModel asm;
	private int preAnswerType;
	
	public QuickActionDialog(Context context){
		this.context = context;
		
		init();
	}
	
	private void init(){
		ActionItem multiChoiceItem = new ActionItem(TYPE_MULTIPLE_CHOICE, "객관식");
		ActionItem essayFormItem = new ActionItem(TYPE_ESSAY_FORM, "주관식");
		
		ActionItem oneItem = new ActionItem(TYPE_TOTAL_ONE, "1");
		ActionItem twoItem = new ActionItem(TYPE_TOTAL_TWO, "2");
		ActionItem threeItem = new ActionItem(TYPE_TOTAL_THREE, "3");
		ActionItem fourItem = new ActionItem(TYPE_TOTAL_FOUR, "4");
		ActionItem fiveItem = new ActionItem(TYPE_TOTAL_FIVE, "5");
		
		ActionItem hanguelItem = new ActionItem(TYPE_KEYBOARD_HANGUEL, "한글");
		ActionItem englishItem = new ActionItem(TYPE_KEYBOARD_ENGLISH, "영어");
		ActionItem numericItem = new ActionItem(TYPE_KEYBOARD_NUMERIC, "숫자");
		
		mcef_quickAction = new QuickAction(context, QuickAction.ORIENTATION_HORIZONTAL,  QuickAction.COLOUR_DARK);	// 객관식 주관식 quickAction
		mcef_quickAction.addActionItem(multiChoiceItem);
		mcef_quickAction.addActionItem(essayFormItem);
		
		mc_c_quickAction = new QuickAction(context, QuickAction.ORIENTATION_HORIZONTAL,  QuickAction.COLOUR_LIGHT);	// 객관식 답 개수 quickAction
		mc_c_quickAction.addActionItem(oneItem);
		mc_c_quickAction.addActionItem(twoItem);
		mc_c_quickAction.addActionItem(threeItem);
		mc_c_quickAction.addActionItem(fourItem);
		mc_c_quickAction.addActionItem(fiveItem);
		
		ef_kt_quickAction = new QuickAction(context, QuickAction.ORIENTATION_HORIZONTAL,  QuickAction.COLOUR_LIGHT);	// 주관식 키보드 타입 quickAction
		ef_kt_quickAction.addActionItem(hanguelItem);
		ef_kt_quickAction.addActionItem(englishItem);
		ef_kt_quickAction.addActionItem(numericItem);
	}
	
	public void setQuestionNoTextView(TextView qNo_textView){
		this.qNo_textView = qNo_textView;
	}
	
	public QuickAction enableQuickAction(final View convertView, final  AnswerSheetListAdapter adapter, final int position){
		
		
		mcef_quickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				asm = SharedDBManager.getInstance().answer_data_list.get(position);
				preAnswerType = asm.getAnswerType();
				
				if (actionId == TYPE_MULTIPLE_CHOICE) {
					asm.setAnswerType(TYPE_MULTIPLE_CHOICE);
					mc_c_quickAction.show(convertView, null, null, "정답의 수를 설정하세요");
				} else if (actionId == TYPE_ESSAY_FORM) {
					asm.setAnswerType(TYPE_ESSAY_FORM);
					ef_kt_quickAction.show(convertView, null, null, "입력방식을 설정하세요");
					//qNo_textView.setTextColor(Color.BLACK);
				}
				
			}
		});
		
		mcef_quickAction.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				qNo_textView.setTextColor(Color.BLACK);
				if(asm != null)
					asm.setAnswerType(preAnswerType);
			}
		});
		
		mc_c_quickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				
				if (actionId == TYPE_TOTAL_ONE) {
					asm.setTotalAnswerCount(1);	
				} else if(actionId == TYPE_TOTAL_TWO) {
					asm.setTotalAnswerCount(2);
				} else if(actionId == TYPE_TOTAL_THREE) {
					asm.setTotalAnswerCount(3);
				} else if(actionId == TYPE_TOTAL_FOUR) {
					asm.setTotalAnswerCount(4);
				} else if(actionId == TYPE_TOTAL_FIVE) {
					asm.setTotalAnswerCount(5);
				}
				
				qNo_textView.setTextColor(Color.BLACK);
				
				asm.setEssayFormAnswer("");			// 주관식 뷰에 답이 설정되어 있는 것을 모두 초기화
				adapter.notifyDataSetChanged();
			}
		});
		
		mc_c_quickAction.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				qNo_textView.setTextColor(Color.BLACK);
				if(asm != null)
					asm.setAnswerType(preAnswerType);
			}
		});
		
		ef_kt_quickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				if (actionId == TYPE_KEYBOARD_HANGUEL) {
					asm.setKeyBoardType(TYPE_KEYBOARD_HANGUEL);
				} else if(actionId == TYPE_KEYBOARD_ENGLISH) {
					asm.setKeyBoardType(TYPE_KEYBOARD_ENGLISH);
				} else if(actionId == TYPE_KEYBOARD_NUMERIC) {
					asm.setKeyBoardType(TYPE_KEYBOARD_NUMERIC);
				}
				
				qNo_textView.setTextColor(Color.BLACK);
				asm.getMarkingList().clear();		//  객관식 뷰에 답이 설정되어 있는 것을 모두 초기화
				adapter.notifyDataSetChanged();
			}
		});
		
		ef_kt_quickAction.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				qNo_textView.setTextColor(Color.BLACK);
				if(asm != null)
					asm.setAnswerType(preAnswerType);
			}
		});
		
		return mcef_quickAction;
	}
	
	
}
