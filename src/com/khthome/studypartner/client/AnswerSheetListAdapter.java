package com.khthome.studypartner.client;

import static com.khthome.studypartner.constant.Type.TYPE_ESSAY_FORM;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_ENGLISH;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_HANGUEL;
import static com.khthome.studypartner.constant.Type.TYPE_KEYBOARD_NUMERIC;
import static com.khthome.studypartner.constant.Type.TYPE_MULTIPLE_CHOICE;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.khthome.studypartner.R;
import com.khthome.studypartner.model.AnswerModel;
import com.khthome.studypartner.model.AnswerSheetModel;

public class AnswerSheetListAdapter extends BaseAdapter {
	private Context context;
	public ArrayList<AnswerModel> answer_data_list;
	private AnswerSheetModel answerSheet_data;
	private LayoutInflater inflater;
	private int answerNo_count;
	private int midIdx;
	private int bgColor;
	private Handler handler = new Handler();
	private int height;
	private Typeface fontFace;
	
	public AnswerSheetListAdapter(Context context, AnswerSheetModel answerSheet_data, ArrayList<AnswerModel> answer_data_list){
		this.context = context;
		initialize(context, answerSheet_data, answer_data_list);
	}
	
	public void initialize(Context context, AnswerSheetModel answerSheet_data, ArrayList<AnswerModel> answer_data_list){
		inflater = LayoutInflater.from(context);
		
		fontFace =Typeface.createFromAsset(context.getAssets(),"Roboto-Black.ttf");
		
		this.answerSheet_data = answerSheet_data;
		this.answer_data_list = answer_data_list;
		this.answerNo_count = answerSheet_data.getAnswerNoCount();
		
		
		this.answerSheet_data.setAnswerDataList(answer_data_list);
		
		for(int i=1; i<=answerSheet_data.getQuestionTotal(); i++){
			AnswerModel asd = new AnswerModel();
			asd.setQuestionNo(i);
			asd.setAnswerType(TYPE_MULTIPLE_CHOICE);		// DEFAULT ANSWER TYPE
			asd.setTotalAnswerCount(1);
			this.answer_data_list.add(asd);
		}
		
	}
	
	public void setAnswerSheetBackground(int bgColor){
		this.bgColor = bgColor;
	}
	
	public void setMidIdx(int midIdx){
		this.midIdx = midIdx;
	}
	
	public void setItemHeight(int height){
		this.height = height;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return answer_data_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return answer_data_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int curPos = position;
		final AnswerSheetListView lv = (AnswerSheetListView)parent;
		
		ViewHolder viewHolder;
		TextView qNo_textView;
		LinearLayout answerSheet_layout;		
		EditText essayFormView;
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.answersheetdata_view, parent, false);
			qNo_textView = (TextView) convertView.findViewById(R.id.qNo);
			answerSheet_layout = (LinearLayout) convertView.findViewById(R.id.answerSheet_layout);
			essayFormView = (EditText)convertView.findViewById(R.id.essayFormView);
			
			viewHolder = new ViewHolder(qNo_textView, answerSheet_layout, essayFormView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			
			qNo_textView = viewHolder.qNo_textView;
			answerSheet_layout = viewHolder.answerSheet_layout;
			essayFormView = viewHolder.essayFormView;
		}
		
		convertView.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, height));
		
		qNo_textView.setText(String.valueOf(answer_data_list.get(position).getQuestionNo()));
		qNo_textView.setTypeface(fontFace);
		
		
		essayFormView.setOnEditorActionListener(new OnEditorActionListener() {		// 주관식뷰 입력액션에 따른 이벤트 처리 
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				String ef_answer_string = v.getText().toString();
				answer_data_list.get(curPos).setEssayFormAnswer(ef_answer_string);
				
				if(actionId == EditorInfo.IME_ACTION_DONE){							// 키보드 완료버튼을 누르면 다음행으로 이동(리스트 중간 행만 해당)
					if(curPos+1 == midIdx){	// 중간 줄 클릭, 다음 문항으로 이동							
						midIdx += 1;
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								lv.setSelection(curPos-1);
							}
						}, 700);
					}
					
					doEditTextAutoFocus(curPos, v);		// 주관식뷰에 자동 포커스
                }
				return false;
            } 
		});
				
		setEssayFormText(essayFormView, position);										// 주관식뷰에 이전에 입력된 값이 있다면, 해당 텍스트를 설정
		setKeyboardTypeForEssayForm(essayFormView, position);							// 주관식뷰에 키 입력시, 올라오는 키보드 설정

		addAnswerDataViewToLayout(answerSheet_layout, position, parent, convertView);	// layout에 answerview추가
		
		if(answer_data_list.get(position).getAnswerType() == TYPE_MULTIPLE_CHOICE){		// 답안 데이타 리스트에서 뷰 타입이 객관식이라면
			if(answerSheet_layout.getChildCount() > 0){	// answerSheet_layout에 answerdata_view가 추가 되어있으면, 재활용
				recyleAnswerDataView(answerSheet_layout, position, parent);
				
				answerSheet_layout.setVisibility(View.VISIBLE);							// 객관식 뷰를 담은 레이아웃을 Visible 상태로
				essayFormView.setVisibility(View.GONE);
			}
		}else if(answer_data_list.get(position).getAnswerType() == TYPE_ESSAY_FORM){	// 답안 데이타 리스트에서 뷰 타입이 주관식이라면
			answerSheet_layout.setVisibility(View.GONE);								// 주관식 뷰를 담은 레이아웃을 Visible 상태로
			essayFormView.setVisibility(View.VISIBLE);
		}
		
		
		drawMarkingOnAnswerSheet(answerSheet_layout, position);		// 답안지에 마킹
	
		return convertView;
	}
	
	private void setEssayFormText(EditText essayFormView, int position){
		String ef_answer_string = answer_data_list.get(position).getEssayFormAnswer();
		if(ef_answer_string != null || ef_answer_string != ""){
			essayFormView.setText(ef_answer_string);
		}
	}
	
	private void setKeyboardTypeForEssayForm(EditText essayFormView, int position){
		if(answer_data_list.get(position).getKeyBoardType() == TYPE_KEYBOARD_HANGUEL){
			essayFormView.setPrivateImeOptions("defaultInputmode=korean;");
		}else if(answer_data_list.get(position).getKeyBoardType() == TYPE_KEYBOARD_ENGLISH){
			essayFormView.setPrivateImeOptions("defaultInputmode=english;");
		}else if(answer_data_list.get(position).getKeyBoardType() == TYPE_KEYBOARD_NUMERIC){
			essayFormView.setPrivateImeOptions("defaultInputmode=numeric;");
		}
	}
	
	private void doEditTextAutoFocus(int position, TextView v){
		if(answer_data_list.get(position+1).getAnswerType() == TYPE_ESSAY_FORM){		// 현재 선택된 주관식뷰의 다음 행이 또 다시 주관식이라면
			TextView nextField = (TextView)v.focusSearch(View.FOCUS_DOWN);			
			nextField.requestFocus();												// 다음 뷰로 포커스를 맞춤
		}else{											// 객관식 타입이면 키보드 내림
			v.clearFocus();
			InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(),0);
		}
	}
	
	private void addAnswerDataViewToLayout(LinearLayout answerSheet_layout, final int position, final ViewGroup parent, View convertView){
		TextView qNo_textView = (TextView)((LinearLayout)convertView).getChildAt(0);
		if(answerSheet_layout.getChildCount() == 0){		// answerSheet_layout에 answerdata_view가 없다면, 추가
			
			for(int i=1; i<=5; i++){
				final AnswerDataView answerdata_view = new AnswerDataView(this, context, position, i, convertView);
				answerdata_view.setQuestionNoTextView(qNo_textView);
				answerdata_view.setAnswerViewBackground(bgColor);		// set background color
				answerSheet_layout.addView(answerdata_view, new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1f));	
			}
			
			for(int i=answerNo_count+1; i<=5; i++){						// 답란의 수에 따라 AnswerDataView를 Invisible 시킴
				answerSheet_layout.getChildAt(i-1).setVisibility(View.INVISIBLE);
			}
			
		}
	}
	
	private void recyleAnswerDataView(LinearLayout answerSheet_layout, final int position, final ViewGroup parent){
		for(int i=0; i<answerSheet_layout.getChildCount(); i++){
			final AnswerDataView answerdata_view = (AnswerDataView) answerSheet_layout.getChildAt(i);
			answerdata_view.setOnMarkingListener(new OnMarkingListener() {
				@Override
				public void onMarking() {
					// TODO Auto-generated method stub
					if(answer_data_list.get(position).getMarkingList().size() 
							== answer_data_list.get(position).getTotalAnswerCount()-1){		// 정답 개수가 다 채워졌다면
						if(position == midIdx-2){		// 첫째 줄 클릭, 이전 문항으로 이동
							midIdx -= 1;
							goPreviousQuestion(answerdata_view, position, parent);
						}else if(position == midIdx-3){	// 둘째 줄 클릭, 이전 문항으로 이동
							midIdx -= 2;
							goPreviousQuestion(answerdata_view, position, parent);
						}else if(position+1 == midIdx){	// 중간 줄 클릭, 다음 문항으로 이동							
							midIdx += 1;
							goNextQuestion(answerdata_view, position, parent);
						}else if(position == midIdx){	// 넷째 줄 클릭, 다음 문항으로 이동
							midIdx += 2;
							goNextQuestion(answerdata_view, position, parent);
						}else if(position == midIdx+1){	// 마지막 줄 클릭, 다음 문항으로 이동
							midIdx += 3;
							goNextQuestion(answerdata_view, position, parent);
						}
					}
				}
			});
			answerdata_view.setPosition(position);
			answerdata_view.drawNothing();
		}
	}
	
	private void goPreviousQuestion(final AnswerDataView answerdata_view, final int position, final ViewGroup parent){
		final AnswerSheetListView lv = (AnswerSheetListView)parent;
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				lv.setSelection(position-2);
			}
		}, answerdata_view.getMarkingAnimElapsedTime()+400);
	}
	
	private void goNextQuestion(final AnswerDataView answerdata_view, final int position, final ViewGroup parent){
		final AnswerSheetListView lv = (AnswerSheetListView)parent;
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				lv.setSelection(position-1);
			}
		}, answerdata_view.getMarkingAnimElapsedTime()+400);
	}
	
	private void drawMarkingOnAnswerSheet(LinearLayout answerSheet_layout, int position){
		if(answerSheet_layout.getChildCount() != 0){
			if(answer_data_list.get(position).getMarkingList().size() != 0){	// 리스트에 마킹된 답 정보가 있으면
				for(int i=0; i<answer_data_list.get(position).getMarkingList().size(); i++){
					int[] markingNo = getCheckedMarkingNumbers(answer_data_list.get(position));
					((AnswerDataView)answerSheet_layout.getChildAt(markingNo[i]-1)).drawMarkOnSheetWithNoAnim();
				}
			}
			for(int i=0; i<answerSheet_layout.getChildCount(); i++){
				AnswerDataView answerdata_view = (AnswerDataView) answerSheet_layout.getChildAt(i);
				if(answerdata_view.isMarking && answer_data_list.get(position).getMarkingList().size() == 0){		// 뷰가 재활용이 되기 때문에 마킹정보가 없는데도 isMarking값은 true가 나올 수 있음 
					answerdata_view.drawNothing();																	// 마킹을 하지않음
				}
			}
			
		}
	}
	
	private int[] getCheckedMarkingNumbers(AnswerModel asm) {
		int size = asm.getMarkingList().size();
		int[] markingNumbers = new int[size];
		for(int i=0; i<size; i++){
			markingNumbers[i] = asm.getMarkingList().get(i);
		}
		
		return markingNumbers;
	}

	static class ViewHolder {
		TextView qNo_textView;
		LinearLayout answerSheet_layout;
		EditText essayFormView;
		
		public ViewHolder(TextView qNo_textView, LinearLayout answerSheet_layout, EditText essayFormView){
			this.qNo_textView = qNo_textView;
			this.answerSheet_layout = answerSheet_layout;
			this.essayFormView = essayFormView;
		}
	}
	
}
