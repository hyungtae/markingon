package com.khthome.studypartner.client;

import com.khthome.studypartner.R;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class BaseActivity extends ActionBarActivity {

	protected static Typeface mTypeface;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (BaseActivity.mTypeface == null)
            BaseActivity.mTypeface = Typeface.createFromAsset(getAssets(), "Roboto-BoldCondensed.ttf");

        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        setGlobalFont(root);
        
        actionBarIdForAll();
    }
    
    private void setGlobalFont(ViewGroup root) {
        for (int i = 0; i < root.getChildCount(); i++) {
            View child = root.getChildAt(i);
            if (child instanceof TextView)
                ((TextView)child).setTypeface(mTypeface);
            else if (child instanceof ViewGroup)
                setGlobalFont((ViewGroup)child);
        }
    }
    
    private void actionBarIdForAll(){
        int titleId = 0;

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
            titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        }else{
        	// This is the id is from your app's generated R class when ActionBarActivity is used for SupportActionBar
            titleId = R.id.action_bar_title;
        }

        if(titleId>0){
            TextView titleView = (TextView)findViewById(titleId);
            Typeface mTypeface = Typeface.createFromAsset(getAssets(), "Moebius_Bold_kor.ttf");
            titleView.setTypeface(mTypeface);
            titleView.setTextColor(Color.WHITE);
        }
    }
}
