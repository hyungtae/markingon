package com.khthome.studypartner.client;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import co.juliansuarez.libwizardpager.wizard.ui.PageFragmentCallbacks;

import com.khthome.studypartner.R;
import com.khthome.studypartner.model.S1SWizardPage;
import com.khthome.studypartner.model.parcel.S1SPageModel;
/*
 * StepOneSimple = S1S
 */
public class S1SWizardFragment extends Fragment {
	private static final String ARG_KEY = "key";
	public static final String SIMPLE_M_KEY = "simple_m_key";
	
	private PageFragmentCallbacks mCallbacks;
	private String mKey;
	private S1SWizardPage mPage;
	private EditText mEditText_label;
	private EditText mEditText_questionTotal;
	private EditText mEditText_entireTime;
	private S1SPageModel pm;
	
	public S1SWizardFragment(){}
	
	public static S1SWizardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        S1SWizardFragment fragment = new S1SWizardFragment();
        fragment.setArguments(args);
        return fragment;
    }
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if(!(activity instanceof PageFragmentCallbacks)){
			throw new ClassCastException("Activity must implement PageFragmentCallbacks");
		}
		
		mCallbacks = (PageFragmentCallbacks) activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle args = getArguments();
		mKey = args.getString(ARG_KEY);
		mPage = (S1SWizardPage) mCallbacks.onGetPage(mKey);
		
		setEmptyValueToData();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_step1_simple_wizard_page, container, false);
		((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
		
		S1SPageModel pm;
		
		if (savedInstanceState != null && savedInstanceState.getParcelable(SIMPLE_M_KEY) != null)
			pm = savedInstanceState.getParcelable(SIMPLE_M_KEY);
		else
			pm = (S1SPageModel) mPage.getData().getParcelable(SIMPLE_M_KEY);
		
		mEditText_label = (EditText) rootView.findViewById(R.id.s_answerSheet_label);
		mEditText_questionTotal = (EditText) rootView.findViewById(R.id.s_question_total);
		mEditText_entireTime = (EditText) rootView.findViewById(R.id.s_entire_time);
		
		mEditText_questionTotal.setNextFocusDownId(R.id.s_entire_time);    		// code for compatability 2.3
		
		mEditText_label.setText(pm.getLabel());
		mEditText_questionTotal.setText(pm.getQuestion_total());
		mEditText_entireTime.setText(pm.getEntire_time());
		
		return rootView;
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SIMPLE_M_KEY, pm);
    }
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
	    if (savedInstanceState != null && savedInstanceState.getParcelable(SIMPLE_M_KEY) != null) {
            // Restore last state for this view.
	    	pm = savedInstanceState.getParcelable(SIMPLE_M_KEY);
	    }
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setInputType();
		
		mEditText_label.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				pm.setLabel((editable != null) ? editable.toString() : null);
				mPage.getData().putParcelable(SIMPLE_M_KEY, pm);
				mPage.notifyDataChanged();
			}
		});
		
		mEditText_questionTotal.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				String s = editable.toString();
		        if(!s.startsWith("0")){
		        	pm.setQuestion_total((editable != null) ? editable.toString() : null);
					mPage.getData().putParcelable(SIMPLE_M_KEY, pm);
					mPage.notifyDataChanged();
		        }else{
		        	mEditText_questionTotal.setText("");
		        }
				
			}
		});
		
		mEditText_entireTime.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable editable) {
				String s = editable.toString();
		        if(!s.startsWith("0")){
		        	pm.setEntire_time((editable != null) ? editable.toString() : null);
					mPage.getData().putParcelable(SIMPLE_M_KEY, pm);
					mPage.notifyDataChanged();
		        }else{
		        	mEditText_entireTime.setText("");
		        }
			}
		});

	}
	
	private void setEmptyValueToData(){
		pm = new S1SPageModel(); 
		mPage.getData().putParcelable(SIMPLE_M_KEY, pm);
	}
	
	protected void setInputType(){
		mEditText_label.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_label.setPrivateImeOptions("defaultInputmode=korean;");
		mEditText_questionTotal.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		mEditText_entireTime.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
	}
	
}
