package com.khthome.studypartner.client;

import static com.khthome.studypartner.client.CircleGroupingNumberView.POSITIONED_CENTER;
import static com.khthome.studypartner.client.CircleGroupingNumberView.POSITIONED_END;
import static com.khthome.studypartner.client.CircleGroupingNumberView.POSITIONED_START;
import static com.khthome.studypartner.client.CircleGroupingNumberView.STATE_GROUPING_BEFORE;
import static com.khthome.studypartner.client.CircleGroupingNumberView.STATE_GROUPING_FINISHED;
import static com.khthome.studypartner.client.CircleGroupingNumberView.STATE_IS_GROUPING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.khthome.studypartner.R;
import com.khthome.studypartner.client.CircleGroupingNumberView.OnClickListener;
import com.khthome.studypartner.model.S4DWizardPage;
import com.khthome.studypartner.model.parcel.S3DQuestionHeaderModel;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;
import com.khthome.studypartner.model.parcel.S4DPageModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

public class GroupingGridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {
	private Context context;
	private Activity activity;
	private ArrayList<S2DQuestionModel> list;				// 문항 데이타 모델을 담는 리스트
	private ArrayList<S3DQuestionHeaderModel> headerList = new ArrayList<S3DQuestionHeaderModel>();		// 헤더 데이타모델을 담는 리스트
	private HashMap<String, ArrayList<Integer>> groupMap = new HashMap<String, ArrayList<Integer>>();	// 그룹핑 데이타를 담고 있는 리스트(ArrayList<Integer> = bindedSet_list)
	private ArrayList<Integer> bindedSet_list = new ArrayList<Integer>();
	private S4DWizardPage mPage;
	private LayoutInflater inflater;
	private Typeface typeface;
    private int mNumColumns = 0;
    private int maxSizeOfList = 2;
    private boolean isGrouping;
	private String curSection = "";

	
    public GroupingGridAdapter(Context context, ArrayList<S2DQuestionModel> qmList, ArrayList<S3DQuestionHeaderModel> qhmList){
		this.context = context;
		this.list = qmList;
		this.headerList = qhmList;
		
		inflater = LayoutInflater.from(context);
	}
    
    public GroupingGridAdapter(Context context, Activity activity, ArrayList<S2DQuestionModel> qmList, S4DWizardPage mPage){
		this.context = context;
		this.activity = activity;
		this.list = qmList;
		this.mPage = mPage;
		
		if(qmList != null){
			setDefaultHeader(qmList);			// 타이틀 헤더 설정
		}else{
			this.list = new ArrayList<S2DQuestionModel>();
		}
		
		groupMap.put("null", new ArrayList<Integer>());
		
		inflater = LayoutInflater.from(context);
		typeface = Typeface.createFromAsset(context.getAssets(), "Moebius_Regular_kor.ttf");
	}
    
    public void setDefaultHeader(ArrayList<S2DQuestionModel> qmList){
		S3DQuestionHeaderModel title_qhm = new S3DQuestionHeaderModel();
		title_qhm.sectionName="";
		title_qhm.qmList.addAll(qmList);
		headerList.add(title_qhm);		
    }
    
    public S4DPageModel getParcelableData(){
    	S4DPageModel pm = new S4DPageModel();
    	
    	pm.groupMap.putAll(groupMap);
    	
    	for(int i=0; i<list.size(); i++){	// list객체를 pacelable 객체로 추가
    		S2DQuestionModel qm = new S2DQuestionModel();
    		qm.qNo = list.get(i).qNo;
    		qm.enabled = list.get(i).enabled;
    		qm.selected = list.get(i).selected;
    		qm.sectionName = list.get(i).sectionName;
    		qm.sectionName_pk = list.get(i).sectionName_pk;
    		qm.position = list.get(i).position;
    		qm.state = list.get(i).state;
    		qm.tag = list.get(i).tag;
        	pm.list.add(qm);
    	}
    	
    	for(int i=0; i<headerList.size(); i++){
    		S3DQuestionHeaderModel qhm = new S3DQuestionHeaderModel();
    		qhm.sectionName = headerList.get(i).sectionName;
    		for(int j=0; j<headerList.get(i).qmList.size(); j++){
    			S2DQuestionModel qm = new S2DQuestionModel();
    			qm.qNo = headerList.get(i).qmList.get(j).qNo;
    			qm.selected = headerList.get(i).qmList.get(j).selected;
    			qm.enabled = headerList.get(i).qmList.get(j).enabled;
    			
    			qhm.qmList.add(qm);
    		}
    		
    		pm.headerList.add(qhm);
    	}
    	
    	pm.bindedSet_list.addAll(bindedSet_list);
    	
    	pm.curSection = curSection;
    	pm.isGrouping = isGrouping;

    	
    	return pm;
    }
    
    
    public void setPreviousData(ArrayList<S2DQuestionModel> list, ArrayList<S3DQuestionHeaderModel> headerList){
    	this.list = list;
    	this.headerList = headerList;
    }
    
    public void setPreviousData(ArrayList<S2DQuestionModel> list, ArrayList<S3DQuestionHeaderModel> headerList, ArrayList<Integer> bindedSet_list,
    			HashMap<String, ArrayList<Integer>> groupMap, String curSection, boolean isGrouping){
    	this.list = list;
    	this.headerList = headerList;
    	this.bindedSet_list = bindedSet_list;
    	this.groupMap = groupMap;
    	this.curSection = curSection;
    	this.isGrouping = isGrouping;
    }
    
	public GroupingGridAdapter(Context context, ArrayList<S2DQuestionModel> qmList, ArrayList<S3DQuestionHeaderModel> qhmList, S4DWizardPage mPage){
		this.context = context;
		this.list = qmList;
		this.headerList = qhmList;
		this.mPage = mPage;
		
		inflater = LayoutInflater.from(context);
		typeface = Typeface.createFromAsset(context.getAssets(), "Moebius_Regular_kor.ttf");
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return (list != null? list.size() : 0);
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/**
	 * The OnClickWrapper is needed to reattach SuperToast.OnClickListeners on orientation changes. 
	 * It does this via a unique String tag defined in the first parameter so each OnClickWrapper's tag 
	 * should be unique.
	 */
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		CircleGroupingNumberView qNo_circleGroupNumberView;
		TextView qNo_textView;
		final int startEndIdx = position;
		
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_grouping_grid_item, parent, false);
            
            qNo_circleGroupNumberView = (CircleGroupingNumberView) convertView.findViewById(R.id.qNo);
            qNo_textView = (TextView) convertView.findViewById(R.id.qNoTextView);
            viewHolder = new ViewHolder(qNo_circleGroupNumberView, qNo_textView);
            convertView.setTag(viewHolder);
        }else{
        	viewHolder = (ViewHolder)convertView.getTag();
        	
        	qNo_circleGroupNumberView = viewHolder.qNo_circleNumberView;
        	qNo_textView = viewHolder.qNo_textView;
        }

		qNo_circleGroupNumberView.setTag(list.get(position).tag);
		qNo_circleGroupNumberView.setPosition(list.get(position).position)
			.setState(list.get(position).state).setSelectedView(list.get(position).selected);

		if(list.get(position).state != STATE_GROUPING_FINISHED){
			qNo_circleGroupNumberView.adjustValue(isGrouping);
		}
		
		if(isGrouping){
			if(list.get(position).sectionName_pk.equals(curSection)){
				qNo_circleGroupNumberView.setEnabled(true);
			}else{
				qNo_circleGroupNumberView.setEnabled(false);
			}
		}else{
			qNo_circleGroupNumberView.setEnabled(true);
		}
		
		if(!list.get(position).enabled)
			qNo_circleGroupNumberView.setEnabled(false);
		else
			qNo_circleGroupNumberView.setEnabled(true);
		
		qNo_circleGroupNumberView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isGrouping && !list.get(startEndIdx).sectionName_pk.equals(curSection)){		// 그룹핑 중 다른 영역을 클릭했을 경우, 클릭x
					SuperToast superToast = new SuperToast(activity);
					superToast.setText("같은 영역만 그룹핑 할 수 있습니다");
					superToast.setDuration(SuperToast.Duration.SHORT);
					superToast.setBackground(SuperToast.Background.GREEN);
					superToast.setTextColor(Color.WHITE);
					superToast.show();
					
					return;
				}
				
				String key = (String)v.getTag();
				
				if(key.equals("isgrouping")){				// 그룹핑 중(하나만 클릭), 동일 클릭
					list.get(startEndIdx).tag = "null";
					key = null;
				}
				
				key = key != null ? key : "null";
	
				if(!groupMap.get(key).isEmpty()){
					unbindGroup(key);
				}else{
					if(bindedSet_list.size() < maxSizeOfList){			
						if(bindedSet_list.size() == 0){
							list.get(startEndIdx).position = POSITIONED_START;
						}
						
						if(bindedSet_list.size() == 1 && bindedSet_list.get(0) == startEndIdx){		// 동일한 번호 클릭시, 그룹핑 취소
							cancelGroup(startEndIdx);
						}else{
							bindedSet_list.add(startEndIdx);
						}
						
						if(bindedSet_list.size() == 1){
							list.get(startEndIdx).selected = true;
							list.get(startEndIdx).state = STATE_IS_GROUPING;
							list.get(startEndIdx).tag = "isgrouping";
							isGrouping = true;
							curSection = list.get(startEndIdx).sectionName_pk;
							
							notifyDataSetChanged();
						}
						
						if(bindedSet_list.size() == maxSizeOfList){
							bindGroup();
						}
					}
					
					
				}
				
				
				
				
				
			}
		});
		
		
		qNo_textView.setText(String.valueOf(list.get(position).qNo));
		qNo_textView.setTypeface(typeface);
		
		return convertView;
	}
	
	private void cancelGroup(int startEndIdx){
		list.get(startEndIdx).selected = false;
		list.get(startEndIdx).state = STATE_GROUPING_BEFORE;
		list.get(startEndIdx).position = -1;
		bindedSet_list.clear();
		isGrouping = false;
		
		notifyDataSetChanged();
	}
	
	private void bindGroup(){
		int startIdx = Math.min(bindedSet_list.get(0), bindedSet_list.get(1));
		int endIdx = Math.max(bindedSet_list.get(0), bindedSet_list.get(1));
		String key = startIdx+ "," + endIdx;
		
		checkBindedKey(startIdx, endIdx);
		
		for(int i = startIdx; i <= endIdx; i++){
			if(i == startIdx){
				list.get(i).position = POSITIONED_START;
			}else if(i == endIdx){
				list.get(i).position = POSITIONED_END;
			}else{
				list.get(i).position = POSITIONED_CENTER;	
			}
			
			list.get(i).state = STATE_GROUPING_FINISHED;
			list.get(i).selected = true;
			list.get(i).tag = key;
		}
		
		groupMap.put(key, new ArrayList<Integer>(bindedSet_list));
		bindedSet_list.clear();
		isGrouping = false;

		notifyDataSetChanged();
	}
	
	private void unbindGroup(String key){
		ArrayList<Integer> bindedSet_list = groupMap.get(key);
		int startIdx = Math.min(bindedSet_list.get(0), bindedSet_list.get(1));
		int endIdx = Math.max(bindedSet_list.get(0), bindedSet_list.get(1));
		
		for(int i = startIdx; i <= endIdx; i++){
			list.get(i).position = -1;
			list.get(i).state = STATE_GROUPING_BEFORE;
			list.get(i).selected = false;
			list.get(i).tag = "null";
		}
		
		groupMap.remove(key);
		
		
		notifyDataSetChanged();
	}
	
	private void checkBindedKey(int newKey_min, int newKey_max){
		ArrayList<String> removeKeyList = new ArrayList<String>();
		Iterator<String> iterator = groupMap.keySet().iterator();
		
		while (iterator.hasNext()) {
	        String oldKey = (String) iterator.next();

	        if(!oldKey.equals("null")){
	        	String[] array = oldKey.split(",");
		        int oldKey_min = Integer.parseInt(array[0]);
				int oldKey_max = Integer.parseInt(array[1]);
		        
				if(newKey_min < oldKey_min && newKey_max > oldKey_max){
					removeKeyList.add(oldKey);
				}
	        }
	    }
		
		for(String key : removeKeyList){
        	groupMap.remove(key);
        }
		
	}

	@Override
	public int getCountForHeader(int header) {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.get(header).qmList.size(): 0); 
	}

	@Override
	public int getNumHeaders() {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.size() : 1);	// 1 = default title header
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder viewHolder;
		TextView header_textView;
		FrameLayout lineView;
		
		TextView myText;
		if(position == 0 ){
			myText = new MyFontTextView(new ContextThemeWrapper(context, R.style.WizardPageTitle));
			myText.setText(mPage.getTitle());
			myText.setPadding((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, context.getResources().getDisplayMetrics()), 0, 0, 0);
			
			return myText;
		}else{
			if(convertView != null){		// convertView(타이틀뷰)를 null로 만들고 섹션뷰를 넣기 위한 조건 
				if(!(convertView.getTag() instanceof HeaderViewHolder)){
					convertView = null;
				}
			}
			
			if (convertView == null) {
	            convertView = inflater.inflate(R.layout.header, parent, false);

	            header_textView = (TextView) convertView.findViewById(R.id.header);
	            lineView = (FrameLayout) convertView.findViewById(R.id.line);
	            viewHolder = new HeaderViewHolder(header_textView, lineView);
	            convertView.setTag(viewHolder);
	        }else{
	        	viewHolder = (HeaderViewHolder)convertView.getTag();
	        	
	        	header_textView = viewHolder.header_textView;
	        	lineView = viewHolder.lineView;
	        }
			
			header_textView.setText(headerList.get(position).sectionName);
			
			if(headerList.get(position).sectionName.equals("")){		// dummyTextView의 크기를 없앰
				header_textView.setVisibility(View.GONE);
			}else{
				header_textView.setVisibility(View.VISIBLE);
				lineView.setVisibility(View.VISIBLE);
			}
			
			return convertView;
		}
		
		
	}

    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }
	
	static class ViewHolder {
		CircleGroupingNumberView qNo_circleNumberView;
		TextView qNo_textView;
		
		public ViewHolder(CircleGroupingNumberView qNo_circleNumberView, TextView qNo_textView){
			this.qNo_circleNumberView = qNo_circleNumberView;
			this.qNo_textView = qNo_textView;
		}
	}
	
	static class HeaderViewHolder {
		TextView header_textView;
		FrameLayout lineView;
		
		public HeaderViewHolder(TextView header_textView, FrameLayout lineView){
			this.header_textView = header_textView;
			this.lineView = lineView;
		}
	}

}
