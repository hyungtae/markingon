/*
 Copyright 2013 Tonic Artos

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 

package com.khthome.studypartner.client;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.khthome.studypartner.R;
import com.khthome.studypartner.model.QuestionHeaderModel;
import com.khthome.studypartner.model.QuestionModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.OnHeaderClickListener;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView.OnHeaderLongClickListener;

*//**
 * A list fragment representing a list of Items. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link ItemDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 * 
 * @author Tonic Artos
 *//*
public class QuestionGridFragment extends Fragment implements OnItemClickListener,
        OnHeaderClickListener, OnHeaderLongClickListener {
    private static final String KEY_LIST_POSITION = "key_list_position";

    *//**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     *//*
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(int id, View view) {
        }
    };

    *//**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     *//*
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    *//**
     * The current activated item position. Only used on tablets.
     *//*
    private int mActivatedPosition = ListView.INVALID_POSITION;
    *//**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     *//*
    private Callbacks mCallbacks = sDummyCallbacks;

    private int mFirstVisible;

    private GridView mGridView;

    private Menu mMenu;

    private Toast mToast;

    *//**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     *//*
    public QuestionGridFragment() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_grid, container, false);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onHeaderClick(AdapterView<?> parent, View view, long id) {
        String text = "Header " + ((TextView)view.findViewById(R.id.header)).getText() + " was tapped.";
        if (mToast == null) {
            mToast = Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
        }
        mToast.show();
    }

    @Override
    public boolean onHeaderLongClick(AdapterView<?> parent, View view, long id) {
        String text = "Header " + ((TextView)view.findViewById(R.id.header)).getText() + " was long pressed.";
        if (mToast == null) {
            mToast = Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
        }
        mToast.show();
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> gridView, View view, int position, long id) {
        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        mCallbacks.onItemSelected(position, view);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    QuestionGridAdapter mAdapter;
    View mFrameView;
    int mNumColumns = 5;
    int mImageThumbSpacing = 0;
    int mImageThumbSize = 0;
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGridView = (GridView)view.findViewById(R.id.asset_grid);
        mGridView.setOnItemClickListener(this);

        
         * Currently set in the XML layout, but this is how you would do it in
         * your code.
         
        // mGridView.setColumnWidth((int) calculatePixelsFromDips(100));
        // mGridView.setNumColumns(StickyGridHeadersGridView.AUTO_FIT);
        
        mGridView.setAdapter(new StickyGridHeadersSimpleArrayAdapter<String>(getActivity()
                .getApplicationContext(), getResources().getStringArray(R.array.countries),
                R.layout.header, R.layout.item));
        
        ArrayList<QuestionHeaderModel> qhmList = new ArrayList<QuestionHeaderModel>();
        QuestionHeaderModel listening_qhm = new QuestionHeaderModel();
        QuestionHeaderModel reading_qhm = new QuestionHeaderModel();
        listening_qhm.sectionName = "듣기영역";
        reading_qhm.sectionName = "리딩영역";
        
        ArrayList<QuestionModel> qmList = new ArrayList<QuestionModel>();
        for(int i=1; i<=50; i++){
        	QuestionModel qm = new QuestionModel();
        	qm.qNo = i;
        	qmList.add(qm);
        	
        	if(i<=20){
        		listening_qhm.qmList.add(qm);
        	}else {
				reading_qhm.qmList.add(qm);
			}
		}
        
        qhmList.add(listening_qhm);
        qhmList.add(reading_qhm);
        
        mAdapter = new QuestionGridAdapter(getActivity().getApplicationContext(), qmList, qhmList);
        mGridView.setAdapter(mAdapter);

        View frame = (View)view.findViewById(R.id.frame);

        mFrameView = frame;
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // When we get here, the size of the frame view is known.
                        // Use those dimensions to set the width of the columns and the image adapter size.
                        // Log.d ("Image Squares", "Size of frame (on layout): " 
                        //        + mFrameView.getHeight () + " w: "+ mFrameView.getWidth ());
                        if (mAdapter.getNumColumns() == 0) {
                           View f = mFrameView;
                           int fh  = f.getHeight () 
                                               - f.getPaddingTop () - f.getPaddingBottom ();
                           int shortestWidth = fh;
                           int fw = f.getWidth () - f.getPaddingLeft () - f.getPaddingRight ();
                           if (fw < shortestWidth) shortestWidth = fw;
                           int usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing
                                             - f.getPaddingLeft () - f.getPaddingRight ();
                           //usableWidth = shortestWidth;
                           usableWidth = shortestWidth - (0 + mNumColumns) * mImageThumbSpacing;

                           int columnWidth = usableWidth / mNumColumns;
                           mImageThumbSize = columnWidth; // - mImageThumbSpacing;
                           int gridWidth = shortestWidth;

                           // The columnWidth used is an integer. That means that we usually end up with a
                           // little unused space. Fix that up with padding if the unused space is more than
                           // a half an image.
                           int estGridWidth = mNumColumns * columnWidth;
                           int unusedSpace = (shortestWidth - estGridWidth);
                           boolean addPadding = (unusedSpace * 2) > mImageThumbSize;
                           if (addPadding) {
                              // This is not a precise calculation. Pad with roughly 1/3 the unused space.
                              int pad = unusedSpace / 3;
                              if (pad > 0) mGridView.setPadding (pad, pad, pad, pad);
                              // Log.d ("Image Squares", "Padding to center: " + pad);
                           }

                            //mGridView.setColumnWidth (mImageThumbSize);
                            mGridView.setColumnWidth (columnWidth);

                           //mGridView.setColumnWidth (shortestWidth / mNumColumns);
                           //LayoutParams lparams = new LinearLayout.LayoutParams (usableWidth, usableWidth);

                           // Now that we have made all the extra adjustments, resize the grid
                           // and have it redo its view one more time.
                           LayoutParams lparams = new LinearLayout.LayoutParams (gridWidth, LayoutParams.MATCH_PARENT);
                           mGridView.setLayoutParams (lparams);
                           //Log.d ("Image Squares", "Size of grid: " + mGridView.getHeight () + " w: "+ mGridView.getWidth ());

                           mAdapter.setNumColumns(mNumColumns);
                           // mAdapter.setItemHeight(mImageThumbSize);
                           //Log.d ("Image Squares", "Size of square: " + mImageThumbSize);
                        }
                    }
                });
        
        if (savedInstanceState != null) {
            mFirstVisible = savedInstanceState.getInt(KEY_LIST_POSITION);
        }

        mGridView.setSelection(mFirstVisible);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }

        ((StickyGridHeadersGridView)mGridView).setOnHeaderClickListener(this);
        ((StickyGridHeadersGridView)mGridView).setOnHeaderLongClickListener(this);

        setHasOptionsMenu(true);
    }

    *//**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     *//*
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mGridView.setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                    : ListView.CHOICE_MODE_NONE);
        }
    }

    @SuppressLint("NewApi")
    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            mGridView.setItemChecked(mActivatedPosition, false);
        } else {
            mGridView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    *//**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     *//*
    public interface Callbacks {
        *//**
         * Callback for when an item has been selected.
         *//*
        public void onItemSelected(int position, View view);
    }
}
*/