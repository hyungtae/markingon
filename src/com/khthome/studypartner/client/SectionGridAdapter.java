package com.khthome.studypartner.client;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.OnClickWrapper;
import com.khthome.studypartner.R;
import com.khthome.studypartner.client.CircleSectionNumberView.OnClickListener;
import com.khthome.studypartner.client.SectionDialogFragment.DialogListener;
import com.khthome.studypartner.model.S3DWizardPage;
import com.khthome.studypartner.model.parcel.S3DPageModel;
import com.khthome.studypartner.model.parcel.S3DQuestionHeaderModel;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

public class SectionGridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {
	private Context context;
	private Activity activity;
	private ArrayList<S2DQuestionModel> list;				// 문항 데이타 모델을 담는 리스트
	private ArrayList<S3DQuestionHeaderModel> headerList = new ArrayList<S3DQuestionHeaderModel>();		// 헤더 데이타모델을 담는 리스트
	private ArrayList<Integer> bindedSet_list = new ArrayList<Integer>();		// 섹션을 이루는 첫 문항과 끝 문항 번호를 담는 크기가 최대 크기가 2인 리스트
    private S3DQuestionHeaderModel prev_qhm_copy;			// 복사된 이전 헤더데이타 모델 객체
	private S3DWizardPage mPage;
	private LayoutInflater inflater;
	private Typeface typeface;
    private int mNumColumns = 0;
    private int startIdx = 0;					
    private int prevHeaderIdx;					// headerList에서 이전 헤더데이타모델을 찾을 때 쓰일 index
    private int[] bindedSet_array_copy;			// 임시로 쓸 int 배열로 복사된 bindedSet_list

	
    public SectionGridAdapter(Context context, ArrayList<S2DQuestionModel> qmList, ArrayList<S3DQuestionHeaderModel> qhmList){
		this.context = context;
		this.list = qmList;
		this.headerList = qhmList;
		
		inflater = LayoutInflater.from(context);
	}
    
    public SectionGridAdapter(Context context, Activity activity, ArrayList<S2DQuestionModel> qmList, S3DWizardPage mPage){
		this.context = context;
		this.activity = activity;
		this.list = qmList;
		this.mPage = mPage;
		
		if(qmList != null){
			setDefaultHeader(qmList);			// 타이틀 헤더 설정
			bindedSet_list.add(Integer.valueOf(startIdx));	// 미리 1번 설정
			list.get(startIdx).selected  = true;			// 1번 선택
		}else{
			this.list = new ArrayList<S2DQuestionModel>();
		}
		
		
		inflater = LayoutInflater.from(context);
		typeface = Typeface.createFromAsset(context.getAssets(), "Moebius_Regular_kor.ttf");
	}
    
    private void setDefaultHeader(ArrayList<S2DQuestionModel> qmList){
		S3DQuestionHeaderModel title_qhm = new S3DQuestionHeaderModel();
		title_qhm.sectionName="";
		title_qhm.qmList.addAll(qmList);
		headerList.add(title_qhm);
		
		prevHeaderIdx = 0;
    }
    
    public S3DPageModel getParcelableData(){
    	S3DPageModel pm = new S3DPageModel();
    	
    	for(int i=0; i<list.size(); i++){	// list객체를 pacelable 객체로 추가
    		S2DQuestionModel qm = new S2DQuestionModel();
    		qm.qNo = list.get(i).qNo;
    		qm.enabled = list.get(i).enabled;
    		qm.selected = list.get(i).selected;
        	pm.list.add(qm);
    	}
    	
    	for(int i=0; i<headerList.size(); i++){
    		S3DQuestionHeaderModel qhm = new S3DQuestionHeaderModel();
    		qhm.sectionName = headerList.get(i).sectionName;
    		for(int j=0; j<headerList.get(i).qmList.size(); j++){
    			S2DQuestionModel qm = new S2DQuestionModel();
    			qm.qNo = headerList.get(i).qmList.get(j).qNo;
    			qm.selected = headerList.get(i).qmList.get(j).selected;
    			qm.enabled = headerList.get(i).qmList.get(j).enabled;
    			
    			qhm.qmList.add(qm);
    			
    		}
    		
    		pm.headerList.add(qhm);
    	}
    	
    	pm.bindedSet_list.addAll(bindedSet_list);
    	pm.startIdx = startIdx;
    	pm.prevHeaderIdx = prevHeaderIdx;
    	
    	return pm;
    }
    
    public void setPreviousData(ArrayList<S2DQuestionModel> list, ArrayList<S3DQuestionHeaderModel> headerList,
    		ArrayList<Integer> bindedSet_list, int startIdx, int prevHeaderIdx){
    	this.list = list;
    	this.headerList = headerList;
    	this.bindedSet_list = bindedSet_list;
    	this.startIdx = startIdx;
    	this.prevHeaderIdx = prevHeaderIdx;
    }
    
	public SectionGridAdapter(Context context, ArrayList<S2DQuestionModel> qmList, ArrayList<S3DQuestionHeaderModel> qhmList, S3DWizardPage mPage){
		this.context = context;
		this.list = qmList;
		this.headerList = qhmList;
		this.mPage = mPage;
		
		inflater = LayoutInflater.from(context);
		typeface = Typeface.createFromAsset(context.getAssets(), "Moebius_Regular_kor.ttf");
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return (list != null? list.size() : 0);
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/**
	 * The OnClickWrapper is needed to reattach SuperToast.OnClickListeners on orientation changes. 
	 * It does this via a unique String tag defined in the first parameter so each OnClickWrapper's tag 
	 * should be unique.
	 */
	OnClickWrapper onClickWrapper = new OnClickWrapper("superactivitytoast", new SuperToast.OnClickListener() {

	    @Override
	    public void onClick(View view, Parcelable token) {
	        
	        
	        if (startIdx == -1) {							// 마지막 섹션설정에서 취소했을 때는 dummy_qhm이 없음
	        	headerList.remove(headerList.size()-1);		// remove new_qhm
			}else{
				headerList.remove(headerList.size()-1);		// remove dummy_qhm
		        headerList.remove(headerList.size()-1);		// remove new_qhm
			}
	        headerList.add(prev_qhm_copy);				// 이전 헤더섹션정보 객체를 헤더리스트에 넣음(복구)
	        
	        unbindSection();
	        superActivityToast.dismiss();
	    }

	});
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		CircleSectionNumberView qNo_circleNumberView;
		TextView qNo_textView;
		
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_section_grid_item, parent, false);
            
            qNo_circleNumberView = (CircleSectionNumberView) convertView.findViewById(R.id.qNo);
            qNo_textView = (TextView) convertView.findViewById(R.id.qNoTextView);
            viewHolder = new ViewHolder(qNo_circleNumberView, qNo_textView);
            convertView.setTag(viewHolder);
        }else{
        	viewHolder = (ViewHolder)convertView.getTag();
        	
        	qNo_circleNumberView = viewHolder.qNo_circleNumberView;
        	qNo_textView = viewHolder.qNo_textView;
        }
		
		final int endIdx = position;
		
		if(list.get(position).selected){
			qNo_circleNumberView.setColor(Color.parseColor("#FF4444"));
		}else{
			qNo_circleNumberView.setColor(Color.parseColor("#FFBB33"));
		}
		
		if(!list.get(position).enabled)
			qNo_circleNumberView.setEnabled(false);
		else
			qNo_circleNumberView.setEnabled(true);
		
		qNo_circleNumberView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int max_length = 2;
				
				if(bindedSet_list.size() < max_length){
					bindedSet_list.add(endIdx);
					
					((CircleSectionNumberView)v).setColor(Color.parseColor("#FF4444"));
					list.get(endIdx).selected  = true;
					if(bindedSet_list.size() == max_length){
						bindSection();
						
						FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
						SectionDialogFragment sectionDialog = SectionDialogFragment.newInstance();
						
						Bundle data = new Bundle();
						int[] value = {bindedSet_list.get(0), bindedSet_list.get(1)};
						bindedSet_array_copy = value;
						
						data.putIntArray("bindedSet_list", value);
						sectionDialog.setArguments(data);
						sectionDialog.setListener(new DialogListener() {
							
							@Override
							public void onFinishEditDialog(TextView tv) {
								String sectionName = tv.getText().toString();
								
								S3DQuestionHeaderModel dummy_qhm = new S3DQuestionHeaderModel();		// 새로운 섹션이외 나머지 섹션을 나타냄(나누는 역할만함)
								S3DQuestionHeaderModel new_qhm = new S3DQuestionHeaderModel();		// 사용자가 지정한 헤더섹션 정보를 담는 객체
								new_qhm.sectionName = sectionName;
								dummy_qhm.sectionName = "";
								
								S3DQuestionHeaderModel prev_qhm = headerList.get(prevHeaderIdx);
								
								try {
									prev_qhm_copy = (S3DQuestionHeaderModel) prev_qhm.clone();		// 이전 섹션헤더 객체를 복사
								} catch (CloneNotSupportedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								int prev_startIdx = prev_qhm.qmList.get(0).qNo;
								int prev_endIdx = prev_qhm.qmList.get(prev_qhm.qmList.size()-1).qNo;
								
								for(int i=prev_startIdx; i <= prev_endIdx; i++){
									
									if(i > startIdx && i <= endIdx+1){
										S2DQuestionModel qm = prev_qhm.qmList.get(0);
										new_qhm.qmList.add(qm);
										prev_qhm.qmList.remove(0);
									}else{
										S2DQuestionModel qm = prev_qhm.qmList.get(0);
										dummy_qhm.qmList.add(qm);
										prev_qhm.qmList.remove(0);
									}
								}
								
								if(headerList.get(prevHeaderIdx).sectionName.equals("") && !headerList.get(prevHeaderIdx).equals(headerList.get(0))){
									headerList.remove(headerList.size()-1);
								}
								
								headerList.add(new_qhm);
								headerList.add(dummy_qhm);
								
								prevHeaderIdx = headerList.size() - 1;				
								startIdx = (endIdx + 1 == prev_endIdx ? -1 : endIdx + 1);		// -1 = 마지막 섹션을 설정했을 시, 다음 인덱스는 없다라는 의미
								
								if(startIdx == -1){
									headerList.remove(dummy_qhm);
								}
								
								notifyDataSetChanged();
								
								prepareNextBind();
								showUndoToast();
							}

							@Override
							public void onCancelDialog() {
								unbindSection();
							}
						});
						
						// sectionDialog.show(fm, "section_dialog");
						CircleSectionNumberView cnv = (CircleSectionNumberView) v;
						cnv.delegatedToShowDialog(sectionDialog, fm);		// showDialog를 cnv에 위임
					}
				}
			}
		});
		
		qNo_textView.setText(String.valueOf(list.get(position).qNo));
		qNo_textView.setTypeface(typeface);
		
		return convertView;
	}
	
	private void bindSection(){
		int endIdx = Math.max(bindedSet_list.get(0), bindedSet_list.get(1));
		
		for(int i = startIdx; i <= endIdx; i++){
			list.get(i).selected = true;
		}
		
		notifyDataSetChanged();
	}
	
	private void unbindSection(){
		if(bindedSet_list.size()  == 1 || startIdx == -1){		// undo 실행할 때
			int startIdx = Math.min(bindedSet_array_copy[0], bindedSet_array_copy[1]);
			int endIdx = Math.max(bindedSet_array_copy[0], bindedSet_array_copy[1]);
			
			for(int i = startIdx; i <= endIdx; i++){
				list.get(i).selected = false;
				list.get(i).enabled = true;
			}
			
			if(this.startIdx != -1)				
				list.get(this.startIdx).selected  = false;			
			this.startIdx = startIdx;
			list.get(this.startIdx).selected  = true;
			prevHeaderIdx = prevHeaderIdx - 1;
			
			bindedSet_list.clear();
			bindedSet_list.add(Integer.valueOf(this.startIdx));
			
		}else{
			int startIdx = Math.min(bindedSet_list.get(0), bindedSet_list.get(1));
			int endIdx = Math.max(bindedSet_list.get(0), bindedSet_list.get(1));
			
			for(int i = startIdx + 1; i <= endIdx; i++){
				list.get(i).selected = false;
			}
			
			bindedSet_list.remove(Integer.valueOf(endIdx));
		}
		notifyDataSetChanged();
		
	}
	
	private void prepareNextBind(){
		int startIdx = Math.min(bindedSet_list.get(0), bindedSet_list.get(1));
		int endIdx = Math.max(bindedSet_list.get(0), bindedSet_list.get(1));
		
		for(int i = startIdx; i <= endIdx; i++){
			list.get(i).selected = false;
			list.get(i).enabled = false;
		}
		
		bindedSet_list.clear();
		
		if(this.startIdx != -1){
			bindedSet_list.add(Integer.valueOf(this.startIdx)); 
			list.get(this.startIdx).selected  = true;
		}
		
		notifyDataSetChanged();
	}

	@Override
	public int getCountForHeader(int header) {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.get(header).qmList.size(): 0); 
	}

	@Override
	public int getNumHeaders() {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.size() : 1);	// 1 = default title header
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder viewHolder;
		TextView header_textView;
		FrameLayout lineView;
		
		TextView myText;
		if(position == 0 ){
			myText = new MyFontTextView(new ContextThemeWrapper(context, R.style.WizardPageTitle));
			myText.setText(mPage.getTitle());
			myText.setPadding((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, context.getResources().getDisplayMetrics()), 0, 0, 0);
			
			return myText;
		}else{
			if(convertView != null){		// convertView(타이틀뷰)를 null로 만들고 섹션뷰를 넣기 위한 조건 
				if(!(convertView.getTag() instanceof HeaderViewHolder)){
					convertView = null;
				}
			}
			
			if (convertView == null) {
	            convertView = inflater.inflate(R.layout.header, parent, false);

	            header_textView = (TextView) convertView.findViewById(R.id.header);
	            lineView = (FrameLayout) convertView.findViewById(R.id.line);
	            viewHolder = new HeaderViewHolder(header_textView, lineView);
	            convertView.setTag(viewHolder);
	        }else{
	        	viewHolder = (HeaderViewHolder)convertView.getTag();
	        	
	        	header_textView = viewHolder.header_textView;
	        	lineView = viewHolder.lineView;
	        }
			
			header_textView.setText(headerList.get(position).sectionName);
			
			if(headerList.get(position).sectionName.equals("")){		// dummyTextView의 크기를 없앰
				header_textView.setVisibility(View.GONE);
				if(position == 1 && startIdx == 0)						// 첫 영역설정하고 undo할 때, 위에 생신 선 없앰
					lineView.setVisibility(View.GONE);
			}else{
				header_textView.setVisibility(View.VISIBLE);
				lineView.setVisibility(View.VISIBLE);
			}
			
			return convertView;
		}
		
		
	}
	
	public SuperActivityToast getSuperToast(){
		return superActivityToast;
	}
	
	SuperActivityToast superActivityToast;
	private void showUndoToast(){
		superActivityToast = new SuperActivityToast(activity, SuperToast.Type.BUTTON);
		superActivityToast.setDuration(SuperToast.Duration.MEDIUM);
		superActivityToast.setText("Some action performed.");
		superActivityToast.setBackground(SuperToast.Background.GREEN);
		superActivityToast.setTextColor(Color.WHITE);
		superActivityToast.setButtonIcon(SuperToast.Icon.Dark.UNDO, "UNDO");
		superActivityToast.setOnClickWrapper(onClickWrapper);
		superActivityToast.show();
	}

    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }
	
	static class ViewHolder {
		CircleSectionNumberView qNo_circleNumberView;
		TextView qNo_textView;
		
		public ViewHolder(CircleSectionNumberView qNo_circleNumberView, TextView qNo_textView){
			this.qNo_circleNumberView = qNo_circleNumberView;
			this.qNo_textView = qNo_textView;
		}
	}
	
	static class HeaderViewHolder {
		TextView header_textView;
		FrameLayout lineView;
		
		public HeaderViewHolder(TextView header_textView, FrameLayout lineView){
			this.header_textView = header_textView;
			this.lineView = lineView;
		}
	}

}
