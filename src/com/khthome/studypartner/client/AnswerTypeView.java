package com.khthome.studypartner.client;

import static com.khthome.studypartner.constant.ApplicationConsts.TAG;
import static com.khthome.studypartner.constant.Type.TYPE_EX_ALPHABETICAL_1;
import static com.khthome.studypartner.constant.Type.TYPE_EX_ALPHABETICAL_2;
import static com.khthome.studypartner.constant.Type.TYPE_EX_ARITHMETIC;
import static com.khthome.studypartner.constant.Type.TYPE_EX_CUSTOM;
import static com.khthome.studypartner.constant.Type.TYPE_EX_HANGEUL_1;
import static com.khthome.studypartner.constant.Type.TYPE_EX_HANGEUL_2;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.khthome.studypartner.exception.StudyPartnerException;
import com.khthome.studypartner.utils.TypeConverter;

public class AnswerTypeView extends View {
	private Context context;
	private RectF  roundRect;
	private String marking_no;			// current marking number
	private int bgColor;
	private int type;
	private int pixelSize;
	private int text_sp_size = 17;
	private Paint paint;
	private int roundPx;
	private Typeface face;
	
	public AnswerTypeView(Context context, int marking_no, int type) {
		super(context);
		init(context, marking_no, type);
	}
	
	private void init(Context context, int marking_no, int type){
		this.context = context;
		this.type = type;
		
		Typeface fontFace=Typeface.createFromAsset(context.getAssets(),"Moebius_Regular_kor.ttf");
		face = Typeface.create(fontFace, Typeface.BOLD);
		
		try {
			switch(type){
			case TYPE_EX_ARITHMETIC:
				this.marking_no = TypeConverter.getInstance().convertToArithmeticTypeValue(marking_no);
				break;
				
			case TYPE_EX_ALPHABETICAL_1:
				this.marking_no = TypeConverter.getInstance().convertToAlphabeticalTypeOne(marking_no);
				break;
			
			case TYPE_EX_ALPHABETICAL_2:
				this.marking_no = TypeConverter.getInstance().convertToAlphabeticalTypeTwo(marking_no);
				break;
				
			case TYPE_EX_HANGEUL_1:
				this.marking_no = TypeConverter.getInstance().convertToHangeulTypeOne(marking_no);
				break;
				
			case TYPE_EX_HANGEUL_2:
				this.marking_no = TypeConverter.getInstance().convertToHangeulTypeTwo(marking_no);
				break;
				
			case TYPE_EX_CUSTOM:
				this.marking_no = TypeConverter.getInstance().convertToCustomType(marking_no);
				break;
			}
		} catch (StudyPartnerException e) {
			e.setOccuredWindow("MainActivity");
			Log.e(TAG, Log.getStackTraceString(e));
		}
		
		roundPx = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
		pixelSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, text_sp_size, getResources().getDisplayMetrics());
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		
		float left = (float)getMeasuredWidth()/4+getMeasuredWidth()/11;
		float top = (float)getMeasuredHeight()/4;
		float right = (float)getMeasuredWidth()/2+getMeasuredWidth()/4-getMeasuredWidth()/11;
		float bottom = (float)getMeasuredHeight()/2+getMeasuredHeight()/4;
		Log.d(TAG, "AnswerTypeView's (left, top, right, bottom) : ("+ left+", "+top+", "+right+", "+bottom+")");
		RectF bRoundRect = new RectF(left, top, right, bottom);
		roundRect = bRoundRect;		
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		// TODO Auto-generated method stub
		// Log.d(TAG, "onLayout("+changed+","+left+","+top+","+right+","+bottom+")");
		super.onLayout(changed, left, top, right, bottom);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if(paint == null)
			paint = new Paint();		// back side paint
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.FILL);
		
		canvas.drawColor(bgColor);	// view's background
		canvas.drawRoundRect(roundRect, roundPx, roundPx, paint);
		paint.setColor(Color.WHITE); 	// text paint
		paint.setTextSize(pixelSize);
		paint.setTextAlign(Align.CENTER);
		
		paint.setTypeface(face);
		canvas.drawText(marking_no, canvas.getWidth()/2, (canvas.getHeight()/2)-((paint.descent() + paint.ascent()) / 2), paint); 
	}
	
	public void setAnswerViewBackground(int bgColor){
		this.bgColor = bgColor;
	}

}
