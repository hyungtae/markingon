package com.khthome.studypartner.client;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ActionMode;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.khthome.studypartner.R;
import com.khthome.studypartner.client.CircleQuestionDetailNumberView.OnClickListener;
import com.khthome.studypartner.client.CircleQuestionDetailNumberView.OnLongClickListener;
import com.khthome.studypartner.client.SingleQuestionDetailDialogFragment.DialogListener;
import com.khthome.studypartner.model.S2DWizardPage;
import com.khthome.studypartner.model.parcel.S2DPageModel;
import com.khthome.studypartner.model.parcel.S2DQuestionModel;
import com.khthome.studypartner.model.parcel.S3DQuestionHeaderModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

public class QuestionDetailGridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {
	private Context context;
	private Activity activity;
	private ArrayList<S2DQuestionModel> list;				// 문항 데이타 모델을 담는 리스트
	private ArrayList<S3DQuestionHeaderModel> headerList = new ArrayList<S3DQuestionHeaderModel>();		// 헤더 데이타모델을 담는 리스트
	private LayoutInflater inflater;
	private S2DWizardPage mPage;
	private Typeface typeface;
    private int mNumColumns = 0;
    private ActionMode mActionMode;
    private int selectCount = 0;
    
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.question_setting, menu);
        	
        	mode.setTitle("Select Items");
            mode.setSubtitle("One item selected");
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
               case R.id.action_settings:
	   		        FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
	   				GroupQuestionDetailDialogFragment questionDetailDialog = GroupQuestionDetailDialogFragment.newInstance();
	   				questionDetailDialog.setListener(new GroupQuestionDetailDialogFragment.DialogListener() {
						
						@Override
						public void onFinishEditDialog() {
							// TODO Auto-generated method stub
							deSelectAllItem();
							mActionMode.finish(); // Action picked, so close the CAB
						}
						
						@Override
						public void onCancelDialog() {
							// TODO Auto-generated method stub
							
						}
					});
	   				
	   				questionDetailDialog.setQuestionModelList(list);
	   				questionDetailDialog.show(fm, "문항 세부설정");
            	   
            	    
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
        	deSelectAllItem();
            mActionMode = null;
            selectCount = 0;
        }
    };
    
	public QuestionDetailGridAdapter(Context context, Activity activity, ArrayList<S2DQuestionModel> qmList, S2DWizardPage mPage){
		this.context = context;
		this.activity = activity;
		this.list = qmList;
		this.mPage = mPage;
		
		if(qmList != null){
			setDefaultHeader(qmList);			// 타이틀 헤더 설정
		}else{
			this.list = new ArrayList<S2DQuestionModel>();
		}
		
		inflater = LayoutInflater.from(context);
		typeface = Typeface.createFromAsset(context.getAssets(), "Moebius_Regular_kor.ttf");
	}
	
	private void setDefaultHeader(ArrayList<S2DQuestionModel> qmList){
		S3DQuestionHeaderModel title_qhm = new S3DQuestionHeaderModel();
		title_qhm.sectionName="";
		title_qhm.qmList.addAll(qmList);
		headerList.add(title_qhm);
    }
	
	public S2DPageModel getParcelableData(){
		S2DPageModel pm = new S2DPageModel();
    	
    	for(int i=0; i<list.size(); i++){	// list객체를 pacelable 객체로 추가
    		S2DQuestionModel qm = new S2DQuestionModel();
    		qm.qNo = list.get(i).qNo;
    		qm.answer_type = list.get(i).answer_type;
    		qm.answer_ex_type = list.get(i).answer_ex_type;
    		qm.aet_spinner_position = list.get(i).aet_spinner_position;
    		qm.answer_ex_total = list.get(i).answer_ex_total;
    		qm.answer_total = list.get(i).answer_total;
    		qm.keyboard_type = list.get(i).keyboard_type;
    		qm.mc_question_type = list.get(i).mc_question_type;
    		qm.ef_question_type = list.get(i).ef_question_type;
    		qm.typeChanged = list.get(i).typeChanged;
        	pm.list.add(qm);
    	}
    	
    	return pm;
    }
	
	public void setPreviousData(ArrayList<S2DQuestionModel> list){
    	this.list = list;
    	setDefaultHeader(list);
    }
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return (list != null? list.size() : 0);
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public ActionMode getActionMode(){
		return mActionMode;
	}
	
    public void deSelectAllItem(){
    	for(int i=0; i < list.size(); i++){
	   		if(list.get(i).selected){
	   			list.get(i).selected = false;
	   		}
	   	}
    	notifyDataSetChanged();
    }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		CircleQuestionDetailNumberView qNo_circleNumberView;
		TextView qNo_textView;
		final int idx = position;
		
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_questiondetail_grid_item, parent, false);
            
            qNo_circleNumberView = (CircleQuestionDetailNumberView) convertView.findViewById(R.id.qNo);
            qNo_textView = (TextView) convertView.findViewById(R.id.qNoTextView);
            viewHolder = new ViewHolder(qNo_circleNumberView, qNo_textView);
            convertView.setTag(viewHolder);
        }else{
        	viewHolder = (ViewHolder)convertView.getTag();
        	
        	qNo_circleNumberView = viewHolder.qNo_circleNumberView;
        	qNo_textView = viewHolder.qNo_textView;
        }
		
		if(list.get(position).selected){
			qNo_circleNumberView.setColor(Color.parseColor("#FF4444"));
		}else{
			qNo_circleNumberView.setColor(Color.parseColor("#FFBB33"));
		}
		
		qNo_circleNumberView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mActionMode == null) {		// 단순 클릭일 때, null 아니면 롱클릭
					if(!list.get(idx).selected){
						preventToOpenTwice(true);
						
						final CircleQuestionDetailNumberView cnv = (CircleQuestionDetailNumberView) v;
						
						cnv.setColor(Color.parseColor("#FF4444"));
						
						
						FragmentManager fm = ((FragmentActivity)activity).getSupportFragmentManager();
						SingleQuestionDetailDialogFragment questionDetailDialog = SingleQuestionDetailDialogFragment.newInstance();
						questionDetailDialog.setListener(new DialogListener() {
							
							@Override
							public void onFinishEditDialog() {
								// TODO Auto-generated method stub
								preventToOpenTwice(false);
								cnv.setColor(Color.parseColor("#FFBB33"));
								cnv.delegatedToShowDialog(null, null);
							}
							
							@Override
							public void onCancelDialog() {
								// TODO Auto-generated method stub
								preventToOpenTwice(false);
								cnv.setColor(Color.parseColor("#FFBB33"));
								cnv.delegatedToShowDialog(null, null);
							}
						});
						questionDetailDialog.setSelectedQNo(idx).setQuestionModelList(list);
						
						cnv.delegatedToShowDialog(questionDetailDialog, fm);		// showDialog를 cnv에 위임
					}
		            return;
		        }
				
				if(!list.get(idx).selected){
					((CircleQuestionDetailNumberView)v).setColor(Color.parseColor("#FF4444"));
					list.get(idx).selected = true;
					selectCount++;
				}else{
					((CircleQuestionDetailNumberView)v).setColor(Color.parseColor("#FFBB33"));
					list.get(idx).selected = false;
					selectCount--;
				}
				
				switch (selectCount) {
				case 0:
					mActionMode.finish();
					break;
	            case 1:
	            	mActionMode.setSubtitle("One item selected");
	                break;
	            default:
	            	mActionMode.setSubtitle("" + selectCount + " items selected");
	                break;
	            }
			}
		});
		
		qNo_circleNumberView.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				if (mActionMode != null) {
		            return false;
		        }
				
				CircleQuestionDetailNumberView cnv = (CircleQuestionDetailNumberView) v;
				
		        // Start the CAB using the ActionMode.Callback defined above
		        mActionMode = ((AnswerSheetSettingWizardActivity)activity).startSupportActionMode(mActionModeCallback);
		        if(!list.get(idx).selected)
		        	cnv.setColor(Color.parseColor("#FF4444"));
		        else
		        	cnv.setColor(Color.parseColor("#FFBB33"));
		        list.get(idx).selected = true;
		        selectCount++;
		        
		        return true;
			}
		});
		
		qNo_textView.setText(String.valueOf(list.get(position).qNo));
		qNo_textView.setTypeface(typeface);
		
		return convertView;
	}

	@Override
	public int getCountForHeader(int header) {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.get(header).qmList.size(): 0); 
	}

	@Override
	public int getNumHeaders() {
		// TODO Auto-generated method stub
		return (headerList.size() > 0 ? headerList.size() : 1);	// 1 = default title header
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		TextView myText;
		if(position == 0 ){
			myText = new MyFontTextView(new ContextThemeWrapper(context, R.style.WizardPageTitle));
			myText.setText(mPage.getTitle());
			myText.setPadding((int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, context.getResources().getDisplayMetrics()), 0, 0, 0);
			
			return myText;
		}
		
		return convertView;

	}
	
	private void preventToOpenTwice(boolean flag){
		for(S2DQuestionModel qm : list){
			if(flag)
				qm.selected = true;
			else
				qm.selected = false;
		}
	}

    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }
	
	static class ViewHolder {
		CircleQuestionDetailNumberView qNo_circleNumberView;
		TextView qNo_textView;
		
		public ViewHolder(CircleQuestionDetailNumberView qNo_circleNumberView, TextView qNo_textView){
			this.qNo_circleNumberView = qNo_circleNumberView;
			this.qNo_textView = qNo_textView;
		}
	}

}
