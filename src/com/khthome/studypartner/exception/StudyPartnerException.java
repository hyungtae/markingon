package com.khthome.studypartner.exception;

public class StudyPartnerException extends Exception {
	private static final long serialVersionUID = 1L;
	protected String errorMessage;
	protected String occured_window;
	protected String RELATED_E_TYPE;
	
	public StudyPartnerException(String detailMessage){
		super(detailMessage);
	}
	
	public StudyPartnerException(Throwable throwable){
		super(throwable);
	}
	
	public void setErrorMessage(String errorMessage){
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setOccuredWindow(String occured_window) {
		 this.occured_window = occured_window;
	}
	
	public String getOccuredWindow() {
		return occured_window;
	}
	
	public String getRelatedExceptionType() {
		return RELATED_E_TYPE;
	}
	
	public static String createErrorLog(String errorMessage, String occured_window, String error_from){
		return "["+occured_window+"] "+errorMessage+"... ("+error_from+")";
	}
}
