package com.khthome.studypartner.exception;

import static com.khthome.studypartner.constant.ApplicationConsts.ERROR_FROM_CLIENT;

public class MarkingTypeInvalidException extends StudyPartnerException{
	private static final long serialVersionUID = 1L;

	public MarkingTypeInvalidException(String errorMessage) {
		super(createErrorLog(errorMessage, "", ERROR_FROM_CLIENT));
		this.errorMessage = errorMessage;
		this.occured_window = "";
		this.RELATED_E_TYPE = "";
	}
	
	public MarkingTypeInvalidException(String errorMessage, String occured_window) {
		super(createErrorLog(errorMessage, occured_window, ERROR_FROM_CLIENT));
		this.errorMessage = errorMessage;
		this.occured_window = occured_window;
		this.RELATED_E_TYPE = "";
	}

	public MarkingTypeInvalidException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}
}
